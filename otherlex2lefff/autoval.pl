#!/usr/bin/env perl

my $th = 10;
my $validated_file_name = "";
my $keep_all_reals = 0;
my $no_val_type = 0;

while (1) {
  $_ = shift;
  if (/^$/) {last}
  elsif (/^-m$/) {$validated_file_name = shift}
  elsif (/^-m=(.*)$/) {$validated_file_name = $1}
  elsif (/^-a(?:ll)?$/) {$keep_all_reals = 1}
  elsif (/^-nv$/) {$no_val_type = 1}
  else {die "Unknown option $_"}
}

if ($validated_file_name ne "") {
  open (VAL, "<$validated_file_name") || die "Could not open $validated_file_name: $!";
  while (<VAL>) {
    chomp;
    last if (/^###+\s*END/);
    next if (/^\s*#/);
    /^(.*?\t.*?)\t(.*)/ || die "Wrong format: $_";
    $lemma = $1; $content = $2;
    $manually_validated{$lemma} = 1;
    $lexicon{$lemma}{$content} = 1;
    $lemma2entrynb{$lemma}++;
  }
}

while (<>) {
    chomp;
    next if (/^\s*#/);
    /^(.*?\t.*?)\t(.*)/ || die "Wrong format: $_";
    $lemma = $1; $content = $2;
    next if (defined($manually_validated{$lemma}));
    next if ($lemma =~ /[\(\) ]/); # no compounds or frozen expressions yet
    $lexicon{$lemma}{$content} = 1;
    $lemma2entrynb{$lemma}++;
}

print "################### Entries for lemmas that have only 1 entry in the raw merged lexicon ###################\n";
for $lemma (sort keys %lexicon) {
    $entrynb = $lemma2entrynb{$lemma};
    next unless $entrynb == 1;
    for (keys %{$lexicon{$lemma}}) {
      if ($manually_validated{$lemma}) {
	print "$lemma\t$_\t#1MV#\n";
      } else {
	if (/D__/ && !$keep_all_reals) { # pour une entrée qui a un correspondant dans DV, ce qui est dans Lefff mais pas dans DV est rejeté
	    s/([\(:\|])[^\|\(\),>]*__L/\1/g; s/\|+/\|/g, s/([\(:])\|/\1/g; s/\|([\),])/\1/g;
	}
	s/\_\_[A-Z]//g;
	if (/D__/) {
	    if (/L__/) {
		print "$lemma\t$_";
		print "\t#1LD#" unless $no_val_type;
		print "\n";
	    } else {
		print "$lemma\t$_";
		print "\t#1D#" unless $no_val_type;
		print "\n";
	    }
	} else {
	    print "$lemma\t$_";
	    print "\t#1L#" unless $no_val_type;
	    print "\n";
	}
      }
    }
}

for $i (2..$th) {
  print "################### Entries for lemmas that have $i entries in the raw merged lexicon ###################\n";
  for $lemma (sort keys %lexicon) {
    $entrynb = $lemma2entrynb{$lemma};
    next unless $entrynb == $i;
    $dv_number = 0;
    $lefff_number = 0;
    if ($manually_validated{$lemma}) {
      for (keys %{$lexicon{$lemma}}) {
	print "$lemma\t$_";
	print "\t#${i}MV#" unless $no_val_type;
	print "\n";
      }
    } else {
      for (keys %{$lexicon{$lemma}}) {
	if (/D__/) {
	  $dv_number++;
	}
	if (/L__/) {
	  $lefff_number++;
	}
      }
      if ($dv_number == $i && $lefff_number == $i) {
	for (keys %{$lexicon{$lemma}}) {
	  if (/D__/ && !$keep_all_reals) { # pour une entrée qui a un correspondant dans DV, ce qui est dans Lefff mais pas dans DV est rejeté
	    s/([\(:\|])[^\|\(\),>]*__L/\1/g; s/\|+/\|/g, s/([\(:])\|/\1/g; s/\|([\),])/\1/g;
	  }
	  s/\_\_[A-Z]//g;
	  print "$lemma\t$_";
	  print "\t#$i-${i}OK#" unless $no_val_type;
	  print "\n";
	}
      } elsif ($dv_number == $i || $lefff_number == $i) {
	for (keys %{$lexicon{$lemma}}) {
	  if (/D__/ && !$keep_all_reals) { # pour une entrée qui a un correspondant dans DV, ce qui est dans Lefff mais pas dans DV est rejeté
	    s/([\(:\|])[^\|\(\),>]*__L/\1/g; s/\|+/\|/g, s/([\(:])\|/\1/g; s/\|([\),])/\1/g;
	  }
	  s/\_\_[A-Z]//g;
	  print "$lemma\t$_";
	  print "\t#$i-$i#" unless $no_val_type;
	  print "\n";
	}
      } elsif ($dv_number == 0 || $lefff_number == 0) {
	for (keys %{$lexicon{$lemma}}) {
	  s/\_\_[A-Z]//g;
	  if ($dv_number == 0) {
	    print "$lemma\t$_";
	    print "\t#$i-L#" unless $no_val_type;
	    print "\n";
	  } else {
	    print "$lemma\t$_";
	    print "\t#$i-D#" unless $no_val_type;
	    print "\n";
	  }
	}
      } else {
	for (keys %{$lexicon{$lemma}}) {
	  print "$lemma\t$_";
	  print "\t#$i-val#" unless $no_val_type;
	  print "\n";
	}
      }
    }
  }
}


print "################### Entries for lemmas that have more than $th entries in the raw merged lexicon ###################\n";
for $lemma (sort keys %lexicon) {
  $entrynb = $lemma2entrynb{$lemma};
  next unless $entrynb > $th;
  for (keys %{$lexicon{$lemma}}) {
    #	if (/D__/ && !$keep_all_reals) { # pour une entrée qui a un correspondant dans DV, ce qui est dans Lefff mais pas dans DV est rejeté
    #	    s/([\(:\|])[^\|\(\),>]*__L/\1/g; s/\|+/\|/g, s/([\(:])\|/\1/g; s/\|([\),])/\1/g;
    #	}
    #	s/\_\_[A-Z]//g;
    if ($manually_validated{$lemma}) {
      print "$lemma\t$_";
      print "\t#".$th."+MV#" unless $no_val_type;
      print "\n";
    } else {
      print "$lemma\t$_";
      print "\t#$th+#" unless $no_val_type;
      print "\n";
    }
  }
}

