INFORMATION GENERALE
Script: LGLex2ilex
Auteurs: Elsa Tolone et Benoît Sagot
Organisation: Université Paris-Est, LIGM / INRIA Paris-Rocquencourt, ALPAGE
License: LGPL
Version: 3.4
Date: 2011/10/05
URL: http://infolingu.univ-mlv.fr/

REFERENCE
Tolone, Elsa & Sagot, Benoît (2011). Using Lexicon-Grammar tables for French 
verbs in a large-coverage parser. Edité par Z. Vetulani dans Human Language 
Technology, Forth Language and Technology Conference, LTC 2009, Poznán, 
Poland, November 2009, Revised Selected Papers, Lecture Notes in Artificial 
Intelligence (LNAI). Springer Verlag.

Tolone, Elsa (2011). Analyse syntaxique à l'aide des tables du Lexique-Grammaire 
du français. Thèse de doctorat, LIGM, Université Paris-Est. 340 pp.

DESCRIPTION
LGLex2ilex est un script consacré à convertir le format texte du lexique
LGLex au format Lefff. Il est implémenté en Perl.


USAGE
Usage: lglex2ilex.pl [options] {lefff_files} < [lglex_file] > [ilex_file]
  [lglex_file]
        fichier d'entrée
  [ilex_file]}
        fichier de sortie
  {lefff_files}
        liste de fichiers .ilex du Lefff utilisés pour connaître la classe
        morphologique de chaque entrée

[options]
  -nuc ou --no_unknown_construction
        pour considérer que les constructions inconnues donnent lieu à la
        création d'entrées secondaires distinctes au lieu de créer des
        redistributions inconnues qui ne seront pas déductibles à partir
        de la construction de base

  -e [file] ou --examples [file]
        avec [file] un fichier contenant des exemples pour chaque entrée
        de chaque table, dont le format est :
        <lemma><TAB><table_id><TAB><first_example>(<TAB><other_examples>)
        ce qui permet de récupérer les exemples de ce fichier plutôt que
        ceux des tables

Attention : l'utilisation de ce script requiert la création de la variable
d'environnement TABLESPATH indiquant le chemin du répertoire principal
contenant toutes les données (tables).

Exemples :
  perl lglex2ilex.pl -nuc v.ilex v-phd.ilex < $TABLESPATH/lglex/verbes.lglex.txt > v-lglex.ilex
  perl lglex2ilex.pl -nuc nom.ilex < $TABLESPATH/lglex/noms-predicatifs.lglex.txt > npred-lglex.ilex

Les fichiers v.ilex, v-phd.ilex et nom.ilex font partie du Lefff mais ne
servent ici qu'à attribuer la table de flexion. S'ils ne sont pas présents,
les règles par défaut sont appliquées pour les mots dont la flexion n'est
pas connue. Par exemple, si c'est un mot en -er, on lui attribue la catégorie
de verbe du premier groupe, etc. et par défaut c'est un mot invariable.
