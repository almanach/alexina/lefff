#!/usr/bin/env perl

my $id = "";
my ($lexinfo,$args,$constructions,$lexrules);

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $unhandled_redistributions_are_additional_entries = 0;
my $examples_file = "";
my $v = qr/(?:V|accorder|acquérir|administrer|adresser|allonger|apporter|asséner|attribuer|avoir|commettre|comporter|comprendre|concevoir|connaître|conserver|donner|écoper|effectuer|émettre|encaisser|encourir|endurer|éprouver|essuyer|être|exercer|faire|filer|fixer|flanquer|formuler|garder|imposer|infliger|jeter|lancer|manifester|mettre|obtenir|octroyer|passer|percevoir|perdre|porter|pratiquer|prendre|prescrire|prêter|prononcer|réaliser|recevoir|ressentir|retrouver|subir|tenir|tirer|toucher|provoquer|causer|\(avoir\+perdre\)|Vsup|Vconv)/;
my $det = qr/(?:Dnum|Det\d*|Detc|d'un|un(?: certain)?|des|le|\(le \+ ce \+ son\)|du|Poss\d*)?/;
my $fig = qr/(?:Prép1 Det1 C1 Prép2 Det2 C2|Prép1 Det1 C1 Prépc Detc Cc|Prép1 Det1 Adj1 C1| Prép1 Det1 C1 Adj1|Prép1 Det1 C1|Prép1 C1|Det1 C1 Prép2 Det2 C2|Det1 C1 Prépc Detc Cc|Det1 C1 Adj1|Det1 C1|Poss1 C1|Adj1 Prépc Detc Cc|Adv1 Adj1|Adj1|W|Adv1)/;#Adv1: pas Modif Adv1 car Modif est déjà supprimé par ailleurs #dans l'ordre décroissant
my $adj_re = qr/(?:Adj\d*)/;
my $fig_cf = qr/(?:Det1 N1 Prépc Detc Cc|N1 Prép2 Det2 C2)/;
my $prep_re = qr/(?:Loc|Prép\d?|Prépc|à|de|par|avec|en|pour|contre|vers|dans|auprès de|comme|entre|après|vis-à-vis|d'avec|et|et de|chez|sur|sans)/o;


$usage = <<USAGE;
Usage: lglex2ilex.pl [options] {lefff_files} < [lglex_file] > [ilex_file]

Input [lglex_file] is read on STDIN
Output [ilex_file] is printed on STDOUT
{lefff_files} is a list of .ilex files from the Lefff that are used for knowing the morphological class
  of each entry (no ambiguity resolution yet, hence a problem for the verb 'ressortir').

Options:
\t-nuc, --no_unknown_construction      Consider unknown constructions as defining additional entries,
\t                                     instead of building unknown redistributions for the base construction.
\t-e [file], --examples [file]         Gather examples from [file], whose format is assumed to be:
                                         <lemma><TAB><table_id><TAB><first_example>(<TAB><other_examples>)?
USAGE

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^--?nuc$/i || /^--?no_unknown_construction$/) {$unhandled_redistributions_are_additional_entries=1;}
    elsif (/^--?e(?:x(?:amples?)?)?$/) {$examples_file = shift;}
    elsif (/^--?h(?:elp)?$/) {
      die $usage;
    }
    else {$lefff_files .= " ".$_}
}
$lefff_files =~ s/^ //;

for $lefff_file (split(/ /,$lefff_files)) {
  open (LEFFF_FILE, "<$lefff_file") || die "Could not open $lefff_file";
  binmode LEFFF_FILE, ":utf8";
  while (<LEFFF_FILE>) {
    /^(.*?)(?:__\S*)?\t(.*?)\t/;
    $lemma2infltable{$1} = $2;
  }
}

my %example;
if ($examples_file ne "") {
  open (EXAMPLES, "<$examples_file") || die "Could not open $examples_file: $!";
  binmode EXAMPLES, ":utf8";
  while (<EXAMPLES>) {
    chomp;
    /^(.*?)\t(.*?)\t(.*?)(\t|$)/ || next;
    #$example{$1}{$2} = $3;
    $ex_entry=$1;
    $ex_table=$2;
    $ex_file=$3;
    $ex_entry =~ s/ .*//;
    if ($example{$ex_entry}{$ex_table} ne "") { #si y'a déjà eu un exemple, c'est que l'entrée est duppliquée (sans tenir compte du se...) dans la même table, donc on ne peut savoir quel est le bon exemple
      $example{$ex_entry}{$ex_table} = $ex_file;
#     print STDERR "ex: $ex_entry / $ex_table / $example{$ex_entry}{$ex_table}\n";
    }
  }
  close (EXAMPLES);
}

#  $prep_re = qr/(?:\(E\+Prép\)|Loc|Prép|à|de|par|avec|en|pour|contre|vers|dans|auprès de|comme|entre|après|vis-à-vis|d'avec)/o;

print "# -*- coding: utf-8 -*-\n\n";

while (<>) {
  chomp;
  if (/^ID=([VNC]_.*)/) { # entrée verbale, nominale ou figée
    if ($lexinfo ne "") {
      process_last_entry ();
    }
    $id = $1;
    $lexinfo = "";
    $lexrules = "";
    $args = "";
    $example = "";
    $constructions = "";
  } elsif (/^all-constructions=\[absolute=\((.*)\),verbales=\((.*)\),reductions(?:GN)?=\((.*)\),relative=\((.*)\)\]$/) {
    $constructions = $1;
    $verbales = $2; #pas pris en compte
    $reductions = $3; #pas pris en compte
    $lexrules = $4;
  } elsif (/^all-constructions=\[absolute=\((.*)\),reductions(?:GN)?=\((.*)\),relative=\((.*)\)\]$/) {
    $constructions = $1;
    $verbales = ""; #pas pris en compte
    $reductions = $2; #pas pris en compte
    $lexrules = $3;
  } elsif (/^all-constructions=\[absolute=\((.*)\),relative=\((.*)\)\]$/) {
    $constructions = $1;
    $lexrules = $2;
    $verbales = "";
    $reductions = "";
  } elsif (/^lexical-info=\[(.*)\]$/) {
    $lexinfo = $1;
  } elsif (/^args=\((.*)\)$/) {
    $args = $1;
  } elsif (/^example=\[example="(.*?)"/) {
    $example = $1;
  } elsif (/^To be encoded/) {
    $id = "";
    $lexinfo = "";
  }
}
if ($lexinfo ne "") {
  process_last_entry ();
}

sub process_last_entry {
  $lexinfo =~ /lemma=\"(.*?)\"/ || $lexinfo =~ /complete=\"(.*?)\"/;
  $lemma = $1;
  $reallemma = $lemma;
  $id =~ s/;status=.*$//g;
  $id =~ /^(.)/;
  $cat = $1;
  $macros = "";
  if ($cat eq "V") {
    $reallemma =~ s/^ne //;
    $reallemma =~ s/ .*//;
  } elsif ($cat eq "C") {
    $reallemma =~ s/^ne //;
  }
  for ($i=0;$i<12;$i++) {
    $reallemma =~ s/(^| )<E>//;	#supprime les mots vides de l'entrée (la plus longue entrée des figées comporte 12 mots)
  }
  $reallemma =~ s/^ //g;	#entrées commençant par un espace (figées)
  $reallemma =~ s/' /'/g;	#apostrophes suivies d'un espace (figées)
  $reallemma =~ s/(^| )le ([aeiouéèêh])/\1l'\2/g; #élisions (faute pour les h aspirés =~ 60 mots) (figées)
  $reallemma =~ s/(^| )la ([aeiouéèêh])/\1l'\2/g;
  $reallemma =~ s/(^| )de ([aeiouéèêh])/\1d'\2/g; #(c'est de accord)
  $reallemma =~ s/(^| )à le /\1au /;		  #élisions
  $reallemma =~ s/(^| )à les /\1au /;
  $reallemma =~ s/(^| )de le /\1du /;
  $reallemma =~ s/(^| )de les /\1des /;
  # macros vraies pour toutes les constructions
  if ($cat eq "V") {		#entrée verbale
    $macros .= "cat=v";
    $cat_tot= "v";
    $base_redistr = "\%actif"; # quand on aura la 31I, il faudra être plus malin que ça.
  } elsif ($cat eq "N") {      #entrée nominale
    $macros .= "cat=nc";       #nom commun
    $cat_tot= "cf";	       #constituant figé (séparable du verbe)
    $base_redistr = "\%default";
  } elsif ($cat eq "C") {	#entrée figée (valeurs par défaut)
    $macros .= "";		#pas de cat
    $cat_tot= "cfi";		#constituant figé inséparable du verbe
    $base_redistr = "synt_head=\$1;\%default"; #tete syntaxique = le verbe
    #calcul du nombre de ppv pour savoir le nombre de 0+
    $cpt_ppv = ($lexinfo =~ s/ppv/ppv/g); #/ppv(.*?)="true"/
    if ($lexinfo =~ /neg="true"/) {
      $cpt_ppv++;
    }
  }
  %lightverbs = ();
  %lightverbs2 = ();
  if ($lexinfo =~ /ppvse="true"/) {
    if ($cat =~ /^[VC]$/) {	#pas de "se" pour les noms
      $semid = "se ";
    }
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@pron";
    } else {
      $macros .= ",\@pron";
    }
  } else {
    $semid = "";
  }
  if ($lexinfo =~ /neg="true"/) {
    $semid = "ne pas $semid";
    #cas différent pour les figées
    #if ($cat eq "C") {
    #if($macros eq "") { #cat= vide, pas de virgule
    #$macros .= "clneg =c +";
    #} else {
    #$macros .= ",clneg =c +";
    #}
    #}
    #else {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@neg";
    } else {
      $macros .= ",\@neg";
    }
    #}
  }

  #ajout pour les figées (mais aussi pour les verbes!)
  if ($lexinfo =~ /ca="ça"/) {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@impers_ca";
    } else {
      $macros .= ",\@impers_ca";
    }
  }
  if ($lexinfo =~ /ppven="true"/) {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@pseudo-en";
    } else {
      $macros .= ",\@pseudo-en";
    }
  }
  if ($lexinfo =~ /ppvy="true"/) {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@pseudo-y";
    } else {
      $macros .= ",\@pseudo-y";
    }
  }
  if ($lexinfo =~ /ppvle="true"/) {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@pseudo-le";
    } else {
      $macros .= ",\@pseudo-le";
    }
  }
  if ($lexinfo =~ /ppvla="true"/) {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@pseudo-la";
    } else {
      $macros .= ",\@pseudo-la";
    }
  }
  if ($lexinfo =~ /ppvles="true"/) {
    if ($macros eq "") {	#cat= vide, pas de virgule
      $macros .= "\@pseudo-les";
    } else {
      $macros .= ",\@pseudo-les";
    }
  }

  if ($lexinfo =~ /aux-list=\(etre="true"\)/) {
    if ($macros eq "") { #cat= vide, pas de virgule
      $macros .= "\@être";
    } else {
      $macros .= ",\@être";
    }
  } elsif ($lexinfo =~ /aux-list=\(avoir="true"\)/) {
    if ($macros eq "") { #cat= vide, pas de virgule
      $macros .= "\@avoir";
    } else {
      $macros .= ",\@avoir";
    }
  }
  if ($lexinfo =~ /Vsup=\[cat="verb",list=\(value="(.*?)"\)\]/) {
    for (split (/",value="/,$1)) {
      if ($_ eq "avoir eu") {
        $_="avoir";
      }
      $lightverbs{$_}++ unless (/ /); #ajout de ts les Vsup sauf ceux contenant un espace (exception faite de avoir eu où on garde avoir car c'est le seul Vsup)
    }
  }
  if ($lexinfo =~ /Vconv=\[cat="verb",list=\(value="(.*?)"\)\]/) {
    for (split (/",value="/,$1)) {
      $lightverbs2{$_}++ unless (/ /); #ajout de ts les Vconv sauf ceux contenant un espace
    }
  }
  $semid .= "Lemma";

  %reals = (); %scompl_mood = (); %s_hum = (); %s_nothum = (); %sinf_ctrl = (); %comp_type = (); %orig_columns = ();
  $args =~ s/^const=//;
  for $arg (split (/,const=/, $args)) {
    $arg =~ /pos="(\d)"/;
    $argid = $1;
    $arg =~ /dist=\((.*?\])\)/;
    $dist = $1;
    $dist =~  s/^comp=//;
    for (split (/,comp=/, $dist)) {
      %introds = ();
      if (/origine=\(orig=\"(.*?)\"\)/) {
	for (split (/\",orig=\"/, $1)) {
	  $orig_columns{$_}++;
	}
      }
      if (/introd-loc=\(prep=\"(.*?)\"\)/) {
	@{$introds{Loc}} = split (/\",prep=\"/, $1);
      } else {
	$introds{"Loc"}[0] = "MaybeL";
      }
      if (/introd-prep=\(prep=\"(.*?)\"\)/) {
	@{$introds{"Prép"}} = split (/\",prep=\"/, $1);
      } else {
	$introds{"Prép"}[0] = "MaybeP";
      }
      for $kind (keys %introds) {
	for $introd_id (0..$#{$introds{$kind}}) {
	  $introd = $introds{$kind}[$introd_id];
	  $introd =~ s/!de//;	# pour les Loc-source!de
	  if (/cat="NP"/) {
	    $comp_type{$_} .= "P$argid\[$kind\|$introd\]";
	    #	      print STDERR "! $comp_type{$_}\n";
	    #trait humain/non humain
	    if (/nothum="true"/) { #ordre : chaîne la plus longue en premier
	      $s_nothum{$argid}{nothum}++;      	  
	    } elsif (/hum="true"/) {
	      $s_hum{$argid}{hum}++;        
	    }
	  } elsif (/cat="(comp)"/) {			     #|ceComp supprimé
	    $comp_type{$_} .= "C$argid\[$kind\|$introd\]";   # et le mode?
	    if (/mood="(.*?)"/) {
	      $scompl_mood{$argid}{$1}++; # mouaif
	    }
	  } elsif (/cat="inf"/) {
	    $comp_type{$_} .= "V$argid\[$kind\|$introd\]";
	    if (/contr="(\d)"/) {
	      $sinf_ctrl{$argid}{$1}++; # mouaif
	    }
	  } elsif (/cat="leFaitComp"/) { # pour BS c'est un nom abstrait, il n'a pas besoin de real spéciale
	    $comp_type{$_} .= "P$argid\[$kind\|$introd\]";
	  } elsif (/cat="siPOuSiP"/) {
	    $comp_type{$_} .= "Q$argid\[$kind\|$introd\]";
	  } elsif (/cat="adj"/) {
	    $comp_type{$_} .= "A$argid\[$kind\|$introd\]";
	  }
	}
      }
    }
  }

  $Obj_looks_like_Obja = 0;
  $Obj_looks_like_Objde = 0;
  $Loc_looks_like_Obja = 0;
  $Dloc_looks_like_Objde = 0;
  $Dloc_looks_like_Loc = 0;
  $pcz = 0;
  %ppv = ();
  $lexrules =~ s/^construction=//;
  $redistr_properties = "";
  for $lexrule (split (/,construction=/, $lexrules)) {
    $lexrule =~ s/^"//;
    $lexrule =~ s/"$//;
    $redistr_properties .= " ; $lexrule";
    if ($lexrule eq "Ppv =: le") {
      $ppv{cla} = 1;
    } elsif ($lexrule eq "Ppv =: lui") {
      $ppv{cld} = 1;
    } elsif ($lexrule eq "Ppv =: y") {
      $ppv{y} = 1;
    } elsif ($lexrule eq "Ppv =: en") {
      $ppv{en} = 1;
    } elsif ($lexrule eq "N1 = Qu P =: Ppv") {
      $ppv{1} = 1;
    } elsif ($lexrule eq "à N1 = Ppv =: le") {
      $Obj_looks_like_Obja = 1;
      $ppv{cla} = 1;
    } elsif ($lexrule eq "de N1 = Ppv =: le") {
      $Obj_looks_like_Objde = 1;
      $ppv{cla} = 1;
    } elsif ($lexrule =~ /^(?:$prep_re )?N(\d).* = Ppv$/) {
      $ppv{$1} = 1;
    } elsif ($lexrule =~ /^V\d-inf.* = Ppv$/) {
      $ppv{1} = 1;
    } elsif ($lexrule =~ /^à N\d = là$/) {
      $Loc_looks_like_Obja = 1;
    } elsif ($lexrule =~ /^de N\d = de là$/) {
      $Dloc_looks_like_Objde = 1;
    } elsif ($lexrule eq "Loc N\d =: de N\d source") {
      $Dloc_looks_like_Loc = 1;
      #} elsif ($lexrule eq "[pc z.]") {
      #$pcz = -1;		#prep de la complétive (on sait pas quel est l'argument concerné dans ce cas)
    } elsif ($lexrule eq "Prép N1 =: Prép ce Qu P = Qu P") {
      $pcz = 1;			#prep de l'argument 1
    } elsif ($lexrule eq "Prép N2 =: Prép ce Qu P = Qu P") {
      $pcz = 2;			#prep de l'argument 2
    } elsif ($lexrule eq "[passif par]") {
      $base_redistr .= ",\%passif";
    } elsif ($lexrule eq "[passif de]") {
      $base_redistr .= ",\%passif_de";
    } elsif ($lexrule eq "[extrap]") {
      $base_redistr .= ",\%actif_impersonnel";
      #} elsif ($lexrule eq "[extrap][passif]") {
      #$base_redistr .= ",\%passif_impersonnel";
    } elsif ($lexrule eq "il être Vpp Prép N1 (E+par N0)") {
      $base_redistr .= ",\%passif_impersonnel";	#argument 1 = info perdue
    } elsif ($lexrule eq "il être Vpp Prép N2 (E+par N0)") {
      $base_redistr .= ",\%passif_impersonnel";	#argument 2 = info perdue
    } else {
      $redistr_properties =~ s/ ; [^;]*$//;
    }
  }
  $redistr_properties =~ s/^ ; //;

  @constructions = ();
  %constructions_list = ();
  $constructions =~ s/^construction=//;
  for (split (/,construction=/, $constructions)) {
    s/^"//; s/"$//;
    #s/U/V/g;
    if (s/^true::([^N]|N[^1-9])/\1/) {
      s/\(E\+Prép\)/Prép/;
      $orig_base_constr = $_;
      s/ \(E *\+ *Modif\)//g;	# temporaire
      s/ Modif//g;		# temporaire
      s/ d'un certain/ d'un/g;	# temporaire
      $base_constr = $_;
      #print "base=$base_constr\n";
      last;
    }
  }

  %a_can_be_Prep = ();
  for $step (1,2) {
    %type=();
    $type{$base_constr} = "base";
    $base_var_constrs = "";
    for (split (/,construction=/, $constructions)) {
      s/^"//; s/"$//;
      #s/U/V/g;
      s/ \(E *\+ *Modif\)//g;					# temporaire
      s/ Modif//g;						# temporaire
      s/ d'un certain/ d'un/g;					# temporaire
      if (s/ (un coup de) N( |$)/ N$2/g) {
	$lemma = "$1 $lemma";
      }				     # temporaire
      s/ de \(E\+la part de\)/ de/g; # temporaire
      s/\(E\+Prép\)/Prép/;
      next if ($type{$_} =~ "base");
      s/^.*:://;
      s/ (V[^ ]*) W[^ ]*/ \1/g;

      #constructions traitées autrement ou ignorées
      if ($orig_base_constr eq "N0 $v N1 à N2" && $_ eq "N0 $v N1 Prép N2") {
	$a_can_be_Prep{2} = 1;
	next;
      } elsif ($_ eq "un N être fait par N0") {
	if ($base_redistr !~ /\%passif(,|$)/) {
	  $base_redistr .= ",\%passif";
	}
	next;
      } elsif ($_ =~ /^C.* V$/) {
	unless (defined($warning_already_output{$_})) {
	  $warning_already_output{$_} = 1;
	  print STDERR "WARNING: $id: Incomplete script: construction $_ not handeled\n";
	}      
      }

      #stocke les constructions effectivement traitées
      $constructions_list{$_} = 1;

      if (/^(.*?) ((se )?(?:$v|(?:être |est )(?:Vpp|V-ant|Adj)|V-il))(?: (.*))?$/) {
	$preverb = $1;
	$verb = $2;
	$postverb = $4;
	if ($base_constr !~ /^$preverb $verb/) {
	  $type{$_} = "redistribution";
	} else {
	  $posid = 0;
	  $base_constr_copy = $base_constr;
	  $base_constr_copy =~ s/^$preverb $verb//;
	  while ($postverb =~ s/^\s*(?:($prep_re) )?((?:$det? )?(?:$adj_re )?(?:[CN](\d?)[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?(?: $adj_re)?Qu P(?:ind|subj)?|ce Qu P(?:ind|subj)?|V\d?-inf W))//
		 || $postverb =~ s/^\s*()((?:Advp?|Adj)\d*)//) {
	    $prep = $1;
	    $arg = $2;
	    next if ($prep eq "de" && $arg eq "Nhum"); # pour l'instant, les "de Nhum" sont ignorés ("compléments" du type "Pierre dit _de Marie_ que c'est un canon)
	    next if ($arg =~ / C/ || $arg =~ /^C/);
	    $orig_arg = $prep." ".$arg;
	    $orig_arg =~ s/^ //;
	    #trait humain/non humain
	    $test_nothum=0;
	    $test_hum=0;
	    $test_nr=0;
	    if ($arg =~ /^N(\d?)-hum/) { #ordre : chaîne la plus longue en premier
	      $test_nothum=1;
	    } elsif ($arg =~ /^N(\d?)hum/) {
	      $test_hum=1;
	    } elsif ($arg =~ /^N(\d?)nr/) {
	      $test_nr=1;
	    }
	    $arg =~ s/^((?:(?:$prep_re) )?(?:$det )?(?:$adj_re )?N\d?)[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?$/$1/;
	    $base_arg = "";
	    $success = 0;
	    while ($base_constr_copy !~ /^\s*$/
		   && ($base_constr_copy =~ s/^\s*(?:($prep_re) )?((?:$det )?(?:$adj_re )?(?:[CN]\d?[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?(?: $adj_re)?|Qu P(?:ind|subj)?|ce Qu P(?:ind|subj)?|V\d?-inf W))//
		       || $base_constr_copy =~ s/^\s*()((?:Advp?|Adj)\d*)//)
		  ) {
	      $posid++;
	      $base_prep = $1;
	      $base_arg = $2;
	      next if ($base_arg =~ / C/ || $base_arg =~ /^C/);
	      $base_arg =~ s/^((?:$det )?N\d?)[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?(?: $adj_re)?$/$1/;
	      #	      print STDERR "$base_prep / $prep / $arg\n";
	      if ($base_prep eq $prep && $base_arg eq $arg) {
		$type{$_} .= "I$posid" unless $type{$_} eq "base";
		$success = 2;
	      } elsif ($base_prep eq $prep 
		       #		       && $base_arg =~ /^N\d$/ 
		       && $arg =~ /$det N/) {
		$type{$_} .= "I$posid" unless $type{$_} eq "base";
		$success = 2;
	      } elsif ($base_prep eq $prep && $base_arg =~ /^(?:ce )?Qu P(?:ind|subj)?$/ && $arg =~ /^N\d/) {
		$type{$_} .= "N$posid";
		$success = 1;
		#trait humain/non humain
		if ($test_nothum eq 1) {
		  $s_nothum{$posid}{nothum}++;
		} elsif ($test_hum eq 1) {
		  $s_hum{$posid}{hum}++;
		} elsif ($test_nr eq 1) { #Nhum/N-hum/Qu Pind/Qu Psubj/V-inf W (sans ctrl) = même déf de Nnr que dans LGExtract (même si rarement présent dans les constructions, surtout en conservant l'ordre des argument classique)
		  $s_hum{$posid}{hum}++;
		  $s_nothum{$posid}{nothum}++;
		  $constr{$posid}{core} .= "|scompl|sinf|sn|cln"; # l'ajout de sn|cln est un choix délibéré
		  $scompl_mood{$posid}{ind}++;
		  $scompl_mood{$posid}{subj}++;	#les 2 modes -> ne sont pas gardés ensuite (cf. gestion des macros)
		}
	      } elsif ($base_arg =~ /^V\d?-inf W$/ && $prep eq "" && $arg =~ /^N\d?$/) {
		$type{$_} .= "n$posid";
		$success = 1;
		#trait humain/non humain
		if ($test_nothum eq 1) {
		  $s_nothum{$posid}{nothum}++;
		} elsif ($test_hum eq 1) {
		  $s_hum{$posid}{hum}++;
		} elsif ($test_nr eq 1) { #Nhum/N-hum/Qu Pind/Qu Psubj/V-inf W (sans ctrl) = même déf de Nnr que dans LGExtract (même si rarement présent dans les constructions, surtout en conservant l'ordre des argument classique)
		  $s_hum{$posid}{hum}++;
		  $s_nothum{$posid}{nothum}++;
		  $constr{$posid}{core} .= "|scompl|sinf|sn|cln"; # l'ajout de sn|cln est un choix délibéré
		  $scompl_mood{$posid}{ind}++;
		  $scompl_mood{$posid}{subj}++;	#les 2 modes -> ne sont pas gardés ensuite (cf. gestion des macros)
		}
	      } elsif ($base_prep eq $prep && $base_arg =~ /^V\d?-inf W$/ && $arg =~ /^N\d?$/) {
		$type{$_} .= "p$posid";
		$success = 1;
		#trait humain/non humain
		if ($test_nothum eq 1) {
		  $s_nothum{$posid}{nothum}++;
		} elsif ($test_hum eq 1) {
		  $s_hum{$posid}{hum}++;
		} elsif ($test_nr eq 1) { #Nhum/N-hum/Qu Pind/Qu Psubj/V-inf W (sans ctrl) = même déf de Nnr que dans LGExtract (même si rarement présent dans les constructions, surtout en conservant l'ordre des argument classique)
		  $s_hum{$posid}{hum}++;
		  $s_nothum{$posid}{nothum}++;
		  $constr{$posid}{core} .= "|scompl|sinf|sn|cln"; # l'ajout de sn|cln est un choix délibéré
		  $scompl_mood{$posid}{ind}++;
		  $scompl_mood{$posid}{subj}++;	#les 2 modes -> ne sont pas gardés ensuite (cf. gestion des macros)
		}
	      } elsif ($base_prep ne "" && $prep ne "" && $base_arg eq $arg) {
		$type{$_} .= "S$posid\[$base_prep\|$prep\]";
		$success = 1;
	      } else {
		$type{$_} .= "E$posid";
	      }
	      last if ($success >= 1);
	    }
	    if ($success == 2) {
	      ;			#rien ;)
	    } elsif ($base_arg eq "") {
	      $posid++;
	      $type{$_} .= "E$posid";
	      $base_constr .= " $orig_arg";
	    } elsif ($success == 1) {
	      ;			#rien ;)
	    } elsif ($base_arg ne $arg || $base_prep ne $prep) {
	      $type{$_} = "redistribution";
	      $postverb = "zzz";
	      last;
	    }
	  }
	  if ($postverb =~ /^\s*$/) {
	    while ($base_constr_copy !~ /^\s*$/
		   && ($base_constr_copy =~ s/^\s*((?:(?:$prep_re) )?(?:$det )?(?:$adj_re )?(?:N(\d?)[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?(?: $adj_re)?|Qu P(?:ind|subj)?|ce Qu P(?:ind|subj)?|V\d?-inf W))//
		       || $base_constr_copy =~ s/^\s*(?:Advp?|Adj)\d*()//)
		  ) {
	      $posid++;
	      $base_arg = $1;
	      $type{$_} .= "E$posid";
	    }
	  } else {
	    $type{$_} = "redistribution";
	  }
	}
      } else {
	$type{$_} = "abandoned";
      }
      if ($type{$_} =~ /\d/) {
	$base_var_constrs .= " ; $_";
      }
    }
  }
  $base_var_constrs =~ s/^ ; //;

  $suj_vide=0;			#met aussi la virgue apres Suj
  $obj_vide=0;			#pour metre les <>

  for (keys %constructions_list) {
    %constr = ();

    $constr{origconstr} = $_;

    if ($_ eq $orig_base_constr && $base_constr ne $orig_base_constr) {
      next;
    }
    #    print STDERR "$_\t$type{$_}\t$base_constr\n";

    next if ($type{$_} =~ /\d/ && $_ ne $base_constr);

    do {
      unless (defined($warning_already_output{$_})) {
	$warning_already_output{$_} = 1;
	print STDERR "WARNING: $id: Incomplete script: construction $_ not handeled\n";
      }
      next;
    } unless /^(.*?) ((se )?(?:$v|(?:être |est )(?:Vpp|V-ant|Adj)|V-il))(?: (.*))?$/;
    $preverb = $1;
    $verb = $2;
    if ($cat eq "C" || $cat eq "V") {     #pas de ppv pour les noms 
      $constr{ppv} = $3 || "";
    }
    $postverb = $4;

    #cas des figées avec sujet figé #tester le verbe en premier
    if ($preverb =~ s/Conj Prép0 Det0 C0/Conj Prép0 Det0 C0/ || ($verb eq "V" && $preverb =~ s/Det0 C0/Det0 C0/)) { #comme voici = verbe + actif + pas de sujet (mais il y a un argument) #ne pas supprimer le preverb!
      #Conj Prép0 Det0 C0 #pas de verbe = c0e
      #Conj (Det0 C0+N0) V = cff #pas pris en compte
      #Det0 C0 V = c0 et c0q
      if ($preverb =~ s/Conj Prép0 Det0 C0//) { #en même tps: suppression du preverb!
	$infltable = "inv+";			#pas de verbe = tout invariable
      } elsif ($preverb =~ s/Det0 C0//) {
	#problème: Det0 peut être vide ou comporter plus d'un mot, C0 peut comporter plus d'un mot...
	$infltable = "inv+inv+"; #Det0 et C0 devant le verbe + les ppv (que l'on supprime)
	for ($i=0;$i<$cpt_ppv;$i++) {
	  $infltable .= "0+";
	}
	$infltable .= "0+"; #verbe à l'infinitif + le reste invariable (sauf ppv)
      }
      if ($macros ne "") {
	$macros2 = $macros;	#sauve les info ppv, etc.
	$macros = "cat=v,"; 
	$macros .= $macros2;
      } else {
	$macros = "cat=v";
      }
      $cat_tot= "v";
      $base_redistr = "\%actif"; #synt_head=\$1; = ajouter accolades
      $suj_vide=1;		 #$constr{0}{fs} = "0";
      $obj_vide=1;		 #pour ne pas mettre les <>
    } elsif (($verb eq "être" && $preverb =~ s/Ce/Ce/) || ($verb eq "être" && $preverb =~ s/Det0 C0/Det0 C0/)) { #comme avoir___AUX = verbe + default + ni sujet ni objet (car tout est figé)
      #Ce être = table ec0
      #Det0 C0 être = tables e01 et e0p1
      #$infltable = "inv+";#verbe déjà conjugé = tout invariable -> dans le cas où l'on remet <ENT>est au lieu de <ENT>être dans l'entrée de lglex
      if ($preverb =~ s/Ce//) {
	$infltable = "inv+";	#C0 = ce + les ppv (que l'on supprime)
	for ($i=0;$i<$cpt_ppv;$i++) {
	  $infltable .= "0+";
	}
	$infltable .= "v-être+"; #verbe être à l'infinitif + le reste invariable (sauf ppv)
      } elsif ($preverb =~ s/Det0 C0//) {
	#problème: Det0 peut être vide ou comporter plus d'un mot, C0 peut comporter plus d'un mot...
	$infltable = "inv+inv+"; #Det0 et C0 devant le verbe + les ppv (que l'on supprime)
	for ($i=0;$i<$cpt_ppv;$i++) {
	  $infltable .= "0+";
	}
	$infltable .= "v-être+"; #verbe être à l'infinitif + le reste invariable (sauf ppv)
      }
      if ($macros ne "" && $macros ne "cat=v") { #cas ajouter pour ne pas avoir cat=v,cat=v (non compris)
	$macros2 = $macros;			 #sauve les info ppv, etc.
	$macros = "cat=v,"; 
	$macros .= $macros2;
      } else {
	$macros = "cat=v";
      }
      $cat_tot= "v";
      $base_redistr = "\%default";
      $suj_vide=1;
      $obj_vide=1;			       #pour ne pas mettre les <>
    } elsif ($verb eq "V" && $preverb =~ s/C0//) { #C0 V = table 31I -> verbe impersonnel avec sujet libre
      #$lexinfo =~ /ca=\"(.*?)\"/;
      #$lexinfo =~ /il=\"(.*?)\"/;
      $infltable = "inv"; #verbe à l'infinitif -> sa table va être calculée ensuite (v-er:std)
      if ($macros ne "") {
	$macros2 = $macros;	#sauve les info ppv, etc.
	$macros = "cat=v,"; 
	$macros .= $macros2;
      } else {
	$macros = "cat=v";
      }
      $cat_tot= "v";
      $reals{0}{"sn"}++;
      $base_redistr = "\%actif_impersonnel";
      $reallemma =~ s/ .*//; #les tables de verbes uniquement peuvent contenir Advm...
    }

    if ($id =~ /^N/) {
      $constr{lightverb} = $verb;
    }
    if ($preverb =~ / / && $preverb ne "Qu P") {
      unless (defined($warning_already_output{$_})) {
        $warning_already_output{$_} = 1;
	print STDERR "WARNING: $id: construction not handeled yet (complex preverb '$preverb'): $_\n";
      }
      next;
    }

    if ($verb eq "V" && $id =~ /^N/) {
      unless (defined($warning_already_output{$_})) {
	$warning_already_output{$_} = 1;
	print STDERR "WARNING: $id: verbal construction not handeled within noun tables: $_\n";
      }
      next;
    }
    if ($verb =~ /V-il/) {
      unless (defined($warning_already_output{$_})) {
	$warning_already_output{$_} = 1;
	print STDERR "WARNING: $id: construction not handeled yet (question): $_\n";
      }
      next;
    }
    if ($verb =~ /Vpp/) {
      unless (defined($warning_already_output{$_})) {
	$warning_already_output{$_} = 1;
	print STDERR "WARNING: $id: passive construction ignored: $_\n";
      }
      next;
    }
    if ($verb =~ /V-ant/) {
      unless (defined($warning_already_output{$_})) {
	$warning_already_output{$_} = 1;
	print STDERR "WARNING: $id: derived adjectival construction ignored: $_\n";
      }
      next;
    }

    unless ($unhandled_redistributions_are_additional_entries) {
      # On saute (par "next;") les constructions qui ne doivent pas produire une entrée (mais donc s'intégrer à une autre entrée)
      # Ces constructions sont en fait TOUTES les constructions sauf:
      #   - la construction de base étendue
      #   - les constructions que l'on choisit exprès comme construisant une autre entrée que celle de base
      if ($_ ne $base_constr
	  && !(
	       # les constructions listées ci-dessous sont considérées comme devant faire une entrée séparée
	       /^N1 se $v(?: auprès de N3)? de ce Qu P/
	       || /sur ce point$/
	       || ($orig_base_constr eq "N0 $v N1" && $_ eq "N1 $v") # construction neutre
	       || ($orig_base_constr eq "N0 $v N1 Loc N2 source" && $_ eq "N0 $v N1hum Loc N2abs") # emploi métaphorique
	      )
	 ) {
	#      $tmp = $orig_base_constr;
	$tmp = $base_constr;
	$tmp =~ s/ /_/g;
	$base_redistr .= ",\%".$tmp."__>__";
	$tmp = $constr{origconstr};
	$tmp =~ s/ /_/g;
	$base_redistr .= "$tmp";
	next;
      }
    }

    # on est maintenant sur une construction C qui donnera une entrée (par exemple la construction de base étendue)
    # on va la parser précisément, pour construire le cadre de sous-cat,
    # puis on complètera ce cadre en fonction des infos venant des constructions considérées comme des variantes de C,
    # et en fonction des "comp avec introd"

    $posid = 0;
    $has_already_an_Obl = 0;
    delete $constr{argid2posid};
    delete $constr{posid2argid};
    if ($preverb =~ /^\s*(?:($prep_re) )?(?:($det) )?(?:$adj_re )?(N(\d?)[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?(?: $adj_re)?|Qu P(?:ind|subj)?|ce Qu P(?:ind|subj)?|V\d?-inf W)/) {
      $macroComp=0;
      $macroCtrl=0;
      $constr{$posid}{det} = $2;
      $constr{$posid}{prep} = ();
      $coreprep = $1;
      $core = $3;
      if ($4 ne "") {
	$argid = $4;
      } else {
	$argid = $posid;
      }
      $constr{argid2posid}{$argid} = $posid;
      $constr{posid2argid}{$posid} = $argid;
      if ($core =~ /^N/) {
	$constr{$posid}{core} .= "|sn|cln"; # l'ajout de cln est un choix délibéré
	#trait humain/non humain
	if ($core =~ /^N(\d?)-hum/) { #ordre : chaîne la plus longue en premier
	  $s_nothum{$argid}{nothum}++;
	} elsif ($core =~ /^N(\d?)hum/) {
	  $s_hum{$argid}{hum}++;
	} elsif ($core =~ /^N(\d?)nr/) { #Nhum/N-hum/Qu Pind/Qu Psubj/V-inf W (sans ctrl) = même déf de Nnr que dans LGExtract (même si rarement présent dans les constructions, surtout en conservant l'ordre des argument classique)
	  $s_hum{$argid}{hum}++;
	  $s_nothum{$argid}{nothum}++;
	  $constr{$posid}{core} .= "|scompl|sinf|sn|cln"; # l'ajout de sn|cln est un choix délibéré
	  $scompl_mood{$argid}{ind}++;
	  $scompl_mood{$argid}{subj}++;	#les 2 modes -> ne sont pas gardés ensuite (cf. gestion des macros)
	}
      } elsif ($core =~ /Qu P(ind|subj)?/) {
	$constr{$posid}{core} .= "|scompl|sn|cln"; # l'ajout de sn|cln est un choix délibéré
	$scompl_mood{$argid}{$1}++;
      } elsif ($core =~ /V(\d?)-inf/) {
	$constr{$posid}{core} .= "|sinf|sn|cln"; # l'ajout de sn|cln est un choix délibéré
	$sinf_ctrl{$argid}{$1}++;
      }
    } else {
      $constr{argid2posid}{0} = $posid;	# sécurité (mais bien utile pour les figées avec un N0 qui est un C0)
      $constr{posid2argid}{$posid} = 0;	# sécurité
    }


    #cas des figées: enlève tout ce qui fait partie de l'entrée figée
    if ($cat eq "C" && $postverb =~ s/$fig_cf//) { #a faire en premier
      #c6 -> enlève aussi le N1 après le verbe
      #enpc -> supprime de plus le Det1 figé pour permettre une insertion après le verbe
      $cat_tot= "cf";		#constituant figé (séparable du verbe)
#    } elsif ($cat eq "C" && $postverb =~ s/$fig//) {
    }

    $constr_not1=0;
    while ($postverb =~ s/^\s*(?:($prep_re) )?(?:($det) )?(?:$adj_re )?([CN](\d?)[-\(\)=\w]*(?: source| destination| nv-dest| obl| sur ce point)?(?: $adj_re)?|Qu P(?:ind|subj)?|ce Qu P(?:ind|subj)?|V\d?-inf W)//
	   || $postverb =~ s/^\s*()()((?:Advp?|Adj)\d*)//) {
      next if ($1 eq "de" && $3 eq "Nhum"); # pour l'instant, les "de Nhum" (sans argid) sont ignorés ("compléments" du type "Pierre dit _de Marie_ que c'est un canon)
      next if ($arg =~ / C/ || $arg =~ /^C/);
      $posid++;
      $macroComp=0;
      $macroCtrl=0;
      $constr{$posid}{det} = $2;
      $constr{$posid}{prep} = ();
      $coreprep = $1;
      $core = $3;
      if ($4 ne "") {
	$argid = $4;
      } else {
	$argid = $posid;
      }
      if (defined($constr{argid2posid}{$argid})) {
	for (0..4) {
	  if (!defined($constr{argid2posid}{$_})) {
	    $constr{posid2argid}{$constr{argid2posid}{$argid}} = $_;
	    $constr{argid2posid}{$_} = $constr{argid2posid}{$argid};
	    $sinf_ctrl{$_} = $sinf_ctrl{$constr{argid2posid}{$argid}};
	    delete $sinf_ctrl{$constr{argid2posid}{$argid}};
	    last;
	  }
	}
      }
      $constr{argid2posid}{$argid} = $posid;
      $constr{posid2argid}{$posid} = $argid;
      if ($core =~ /^N/) {
	$constr{$posid}{core} .= "|sn";
	#trait humain/non humain
	if ($core =~ /^N(\d?)-hum/) { #ordre : chaîne la plus longue en premier
	  $s_nothum{$argid}{nothum}++;
	} elsif ($core =~ /^N(\d?)hum/) {
	  $s_hum{$argid}{hum}++;
	} elsif ($core =~ /^N(\d?)nr/) { #Nhum/N-hum/Qu Pind/Qu Psubj/V-inf W (sans ctrl) = même déf de Nnr que dans LGExtract (même si rarement présent dans les constructions, surtout en conservant l'ordre des argument classique)
          $s_hum{$argid}{hum}++;
	  $s_nothum{$argid}{nothum}++;
	  $constr{$posid}{core} .= "|scompl|sinf|sn|cln"; # l'ajout de sn|cln est un choix délibéré"
	  $scompl_mood{$argid}{ind}++;
	  $scompl_mood{$argid}{subj}++;	#les 2 modes -> ne sont pas gardés ensuite (cf. gestion des macros)
	}
      } elsif ($core =~ /Qu P(ind|subj)?/) {
	$constr{$posid}{core} .= "|scompl";
	$scompl_mood{$argid}{$1}++;
      } elsif ($core =~ /V(\d?)-inf/) {
	$constr{$posid}{core} .= "|sinf";
	$sinf_ctrl{$argid}{$1}++;
      }

      #cas des figées: les prépositions sont codées dans lexical-info
      if ($coreprep eq "Prép1") {
	$lexinfo =~ /prep1=\"(.*?)\"/;
	$coreprep = $1;
	#print "prep1:$coreprep\n";
	if ($reallemma =~ s/ $coreprep$//g) { #on enlève la prep de l'entrée (ne fonctionne pas si y'a la 2ème prep aussi)
	} else { #sinon c'est qu'elle est suivi d'un nom, cet argument fait parti de l'entrée, il faut le supprimer du cadre de sous-cat (cf. c0 et c0q)
	  $coreprep = "";
	  $constr_not1=1;
	}
      } elsif ($coreprep eq "Prép2") {
	$lexinfo =~ /prep2=\"(.*?)\"/;
	$coreprep = $1;
	#print "prep2:$coreprep\n";
	$reallemma =~ s/ $coreprep$//g;	#on enlève la prep de l'entrée
      } elsif ($coreprep eq "Prép3") {
	$lexinfo =~ /prep3=\"(.*?)\"/;
	$coreprep = $1;
	#print "prep3:$coreprep\n";
	$reallemma =~ s/ $coreprep$//g;
      }

      $constr{$posid}{core} = sort_reals ($constr{$posid}{core});
      if ($coreprep ne "") {
	$constr{$posid}{core} = "|".$constr{$posid}{core};
	$constr{$posid}{core} =~ s/(?<=\|)([^-]+?)(\||$)/$coreprep-\1\2/g;
	$constr{$posid}{core} =~ s/^\|//;
      }

      #if ($pcz == -1 || $pcz == $posid) { # on suppose que [pc z.] est valide pour TOUS les compléments (sauf le sujet)
      # s'il s'agit de Prép ce Qu P
      #$constr{$posid}{core} =~ s/(^|\|)([^\|]+-)scompl/$1$2scompl|scompl/;
      #$constr{$posid}{core} =~ s/(^|\|)([^\|]+-)qcompl/$1$2qcompl|qcompl/;
      #}

      if ($id =~ /^N/ && $core =~ /N[^\d]*( |$)/) {
	$constr{$posid}{fs} = "0"; ### Attention, ça devrait être moins violent sur les constructions non standard...
      } elsif ($constr_not1 == 0) { #si $constr_not1=1 c'est que l'argument fait parti de l'entrée figée
	if ($posid == 1 && $coreprep eq "" && $table ne "32NM" && $reallemma ne "être") {
	  $constr{$posid}{fs} = "Obj";
	} elsif ($coreprep eq "à") {
	  if ($Obj_looks_like_Obja) {
	    $constr{$posid}{fs} = "Obj";
	  } elsif ($Loc_looks_like_Obja) {
	    $constr{$posid}{fs} = "Loc";
	  } else {
	    $constr{$posid}{fs} = "Objà";
	  }
	} elsif ($coreprep eq "de") {
	  if ($Obj_looks_like_Objde) {
	    $constr{$posid}{fs} = "Obj";
	  } elsif ($Dloc_looks_like_Objde) {
	    $constr{$posid}{fs} = "Dloc";
	  } else {
	    $constr{$posid}{fs} = "Objde";
	  }
	} elsif ($coreprep eq "Loc" && ($core =~ /source/ || $Dloc_looks_like_Loc)) {
	  $constr{$posid}{fs} = "Dloc";
	} elsif ($coreprep eq "Loc") {
	  $constr{$posid}{fs} = "Loc";
	} elsif ($coreprep eq "") {
	  $constr{$posid}{fs} = "Att";
	} else {
	  if ($has_already_an_Obl == 0) {
	    $constr{$posid}{fs} = "Obl";
	    $has_already_an_Obl = 1;
	  } else {
	    $constr{$posid}{fs} = "Obl".++$has_already_an_Obl;
	  }
	}
      }
    }				#fin du while

    for $argid (keys %reals) {
      $constr{$constr{argid2posid}{$argid}}{core} .= "|" . join("|",keys %{$reals{$constr{argid2posid}{$argid}}});
    }

    if ($suj_vide == 0) {
      $constr{0}{fs} = "Suj";
    } else {
      $constr{0}{fs} = "0";	#figée avec sujet figé -> pas de sujet
    }
    $constr{0}{core} = sort_reals ($constr{0}{core});
    $constr{0}{prep} = ();
    $constr{0}{det} = "";
    #    if ($preverb eq "N0hum") {
    #      push (@{$constr{macros}}, "hum = +");
    #    } elsif ($preverb eq "N0-hum") {
    #      push (@{$constr{macros}}, "hum = -");
    #    }


    for (keys %constr) {
      next unless /^\d$/;
      if (!defined($constr{posid2argid}{$_})) {
	delete($constr{$_});
      }
    }

    # fonctions syntaxiques pour les compléments uniquement connus par des comp
    for $posid (keys %constr) {
      next unless $posid =~ /^\d+$/;
      if ($constr{$posid}{fs} eq "") {
	if ($constr{$posid}{core} =~ /^(.*?)-/) {
	  if ($1 eq "à") {
	    $constr{$posid}{fs} = "Objà";
	  } elsif ($1 eq "de") {
	    $constr{$posid}{fs} = "Objde";
	  } else {
	    $constr{$posid}{fs} = "Obl";
	  } 
	}
	$constr{$posid}{opt} = 1;
      }
    }

    # macros spécifiques à la construction
    for $argid (keys %scompl_mood) {
      next unless defined ($constr{argid2posid}{$argid});
      $posid = $constr{argid2posid}{$argid};
      #on ne positionne $mood que si soit ind soit subj est là, mais on laisse $mood vide si y'a les 2
      if (defined($scompl_mood{$argid}{ind}) && !defined($scompl_mood{$argid}{subj})) {
	$mood = "Ind";
      } elsif (defined($scompl_mood{$argid}{subj}) && !defined($scompl_mood{$argid}{ind})) {
	$mood = "Subj";
      } else {
	next;
      }
      if ($constr{$posid}{fs} eq "Suj") {
	push (@{$constr{macros}}, "\@SComp".$mood);
      } elsif ($constr{$posid}{fs} eq "Obj") {
	push (@{$constr{macros}}, "\@Comp".$mood);
      } elsif ($constr{$posid}{fs} eq "Objà") {
	push (@{$constr{macros}}, "\@AComp".$mood);
      } elsif ($constr{$posid}{fs} eq "Objde") {
	push (@{$constr{macros}}, "\@DeComp".$mood);
      }
    }

    #trait humain/non humain
    for $argid (keys %s_nothum) { #si réuni avec hum, pas possible d'avoir les 2 pour la même position
      next unless defined ($constr{argid2posid}{$argid});
      $posid = $constr{argid2posid}{$argid};
      $nothum = "N-hum"; #ou NotHum #pas besoin de vérifier, c'est forcément nothum
      if ($constr{$argid}{fs} eq "Suj" || $constr{$posid}{fs} eq "Suj") {
	push (@{$constr{macros}}, "\@Suj".$nothum);
      } elsif ($constr{$argid}{fs} eq "Obj" || $constr{$posid}{fs} eq "Obj") {
	push (@{$constr{macros}}, "\@Obj".$nothum);
      } elsif ($constr{$argid}{fs} eq "Objà" || $constr{$posid}{fs} eq "Objà") {
	push (@{$constr{macros}}, "\@Objà".$nothum);
      } elsif ($constr{$argid}{fs} eq "Objde" || $constr{$posid}{fs} eq "Objde") {
	push (@{$constr{macros}}, "\@Objde".$nothum);
      } elsif ($constr{$argid}{fs} eq "Loc" || $constr{$posid}{fs} eq "Loc") {
	push (@{$constr{macros}}, "\@Loc".$nothum);
      } elsif ($constr{$argid}{fs} eq "Dloc" || $constr{$posid}{fs} eq "Dloc") {
	push (@{$constr{macros}}, "\@Dloc".$nothum);
      } elsif ($constr{$argid}{fs} eq "Att" || $constr{$posid}{fs} eq "Att") {
	push (@{$constr{macros}}, "\@Att".$nothum);
      } elsif ($constr{$argid}{fs} eq "Obl" || $constr{$posid}{fs} eq "Obl") {
	push (@{$constr{macros}}, "\@Obl".$nothum);
      } elsif ($constr{$argid}{fs} eq "Obl2" || $constr{$posid}{fs} eq "Obl2") {
	push (@{$constr{macros}}, "\@Obl2".$nothum);
      }
    }

    for $argid (keys %s_hum) {
      next unless defined ($constr{argid2posid}{$argid});
      $posid = $constr{argid2posid}{$argid};
      $hum = "Nhum";		#ou Hum
      if ($constr{$argid}{fs} eq "Suj" || $constr{$posid}{fs} eq "Suj") {
	push (@{$constr{macros}}, "\@Suj".$hum);
      } elsif ($constr{$argid}{fs} eq "Obj" || $constr{$posid}{fs} eq "Obj") {
	push (@{$constr{macros}}, "\@Obj".$hum);
      } elsif ($constr{$argid}{fs} eq "Objà" || $constr{$posid}{fs} eq "Objà") {
	push (@{$constr{macros}}, "\@Objà".$hum);
      } elsif ($constr{$argid}{fs} eq "Objde" || $constr{$posid}{fs} eq "Objde") {
	push (@{$constr{macros}}, "\@Objde".$hum);
      } elsif ($constr{$argid}{fs} eq "Loc" || $constr{$posid}{fs} eq "Loc") {
	push (@{$constr{macros}}, "\@Loc".$hum);
      } elsif ($constr{$argid}{fs} eq "Dloc" || $constr{$posid}{fs} eq "Dloc") {
	push (@{$constr{macros}}, "\@Dloc".$hum);
      } elsif ($constr{$argid}{fs} eq "Att" || $constr{$posid}{fs} eq "Att") {
	push (@{$constr{macros}}, "\@Att".$hum);
      } elsif ($constr{$argid}{fs} eq "Obl" || $constr{$posid}{fs} eq "Obl") {
	push (@{$constr{macros}}, "\@Obl".$hum);
      } elsif ($constr{$argid}{fs} eq "Obl2" || $constr{$posid}{fs} eq "Obl2") {
	push (@{$constr{macros}}, "\@Obl2".$hum);
      }
    }

    for $argid (keys %sinf_ctrl) {
      next unless defined ($constr{argid2posid}{$argid});
      $posid = $constr{argid2posid}{$argid};
      if (scalar keys %{$sinf_ctrl{$argid}} > 1) {
	print STDERR "WARNING: entry $id has double controler for infinitive argument no. $argid at position $posid (".join(", ",(sort keys %{$sinf_ctrl{$argid}})).") - skipped\n";
	next;
      }
      for $ctrl_argid (keys %{$sinf_ctrl{$argid}}) { # il y en a forcément un seul...
	$ctrl_posid = $constr{argid2posid}{$ctrl_argid};
        if ($constr{$posid}{fs} ne "0") { #pb venant de la construction N1hum donner Det N à N0...
	  push (@{$constr{macros}}, "\@Ctrl".$constr{$ctrl_posid}{fs}.$constr{$posid}{fs});
        }
      }
    }

    if ($postverb !~ /^\s*$/) {
      unless (defined($warning_already_output{$_})) {
	$warning_already_output{$_} = 2;
	print STDERR "WARNING: $id: construction not handeled yet (postverb not understandable, remainder: $postverb): $_\n";
      }	  
      next;
    }
    %{$constructions[$#constructions+1]} = %constr;
  }				#fin du for





  for $cid (0..$#constructions) {
    next unless $constructions[$cid]{origconstr} eq $base_constr;
    for $type (values %type, values %comp_type) {
      next unless $type =~ /\d/;
      while ($type =~ s/^(.)(.)//) {
	$op = $1; $argid = $2;
	$posid = $constructions[$cid]{argid2posid}{$argid};
	if ($op eq "E") {
	  $constructions[$cid]{$posid}{opt} = 1;
	} elsif ($op eq "N") {
	  $constructions[$cid]{$posid}{core} =~ s/(^|\|)([^\|]+-)?scompl/\1\2scompl\|\2sn/g;
	} elsif ($op eq "n") {
	  $constructions[$cid]{$posid}{core} =~ s/(^|\|)([^\|]+-)?sinf/\1\2sinf\|sn/g;
	} elsif ($op eq "p") {
	  $constructions[$cid]{$posid}{core} =~ s/(^|\|)([^\|]+-)?sinf/\1\2sinf\|\2sn/g;
	} elsif ($op =~ /^[PCVQAS]$/) {
	  #	  print STDERR ">>> $type\n";
	  $type =~ s/\[(.*?)\|(.*?)\]//;
	  $base_prep = $1;
	  $prep = $2;
	  if ($op eq "S") {
	    $constructions[$cid]{$posid}{core} =~ s/(^|\|)($base_prep-)(.*?)(\||$)/\1\2\3\|$prep-\3\4/g;
	  } else {
	    #	    print STDERR "$constructions[$cid]{$posid}{core}\n";
	    if ($base_prep eq "Loc") {
	      $constructions[$cid]{$posid}{core} =~ s/Loc-.*?(\||$)//g;
	      if ($prep eq "Loc-source") {
		$prep = "Loc";
		$constructions[$cid]{$posid}{fs} = "Dloc";
	      }
	    }
	    if ($op eq "P") {
	      $constructions[$cid]{$posid}{core} .= "\|$prep-sn";
	    } elsif ($op eq "C") {
	      $constructions[$cid]{$posid}{core} .= "\|$prep-scompl";
	    } elsif ($op eq "V") {
	      $constructions[$cid]{$posid}{core} .= "\|$prep-sinf";
	    } elsif ($op eq "Q") {
	      $constructions[$cid]{$posid}{core} .= "\|$prep-qcompl";
	    } elsif ($op eq "A") {
	      $constructions[$cid]{$posid}{core} .= "\|$prep-sa";
	    }
	  }
	} elsif ($op eq "I") {
	  # rien
	}
	$constructions[$cid]{$posid}{core} =~ s/^\|//;
      }
    }
  }

  ### gestion des prépositions ###
  # prépositions récupérées de "constructions"
  for $cid (0..$#constructions) {
    for $prepid (keys %a_can_be_Prep) {
      $posid = $constructions[$cid]{argid2posid}{$prepid};
      next unless $constructions[$cid]{$posid}{core} =~ /(à-)(.*)(\||$)/; # à faire: le cas où on a plusieurs à-xxx en alternative
      $tmp = "";
      while ($constructions[$cid]{$posid}{core} =~ /(à-)(.*?)(\||$)/g) {
	$tmp = "Prép-$2|".$tmp;
      }
      $constructions[$cid]{$posid}{core} = $tmp.$constructions[$cid]{$posid}{core};
    }
  }
  # prépositions normales
  $prepositions = "";
  $lexinfo =~ /prepositions=\(([^()]*(?:\([^()]*\)[^()]*)*)\)/;
  $prepositions = $1;
  $prepositions =~ s/preposition=//;
  for $prep (split (/,preposition=/, $prepositions)) {
    $prep =~ /id="(\d)"/;
    $prepid = $1;
    $prep =~ /list=\((.*?)\)/;
    $preplist = $1;
    $preplist =~  s/^prep=//;
    for (split (/,prep=/, $preplist)) {
      s/^.*?"//;
      s/".*//;
      next if ($_ eq "~");
      for $cid (0..$#constructions) {
	$posid = $constructions[$cid]{argid2posid}{$prepid};
	#	print STDERR "$constructions[$cid]{$posid}{core}\n";
	next unless $constructions[$cid]{$posid}{core} =~ /((?:Prép|MaybeP)-)(.*?)(\||$)/;
	$tmp = "";
	while ($constructions[$cid]{$posid}{core} =~ /((?:Prép|MaybeP)-)(.*?)(\||$)/g) {
	  my $local_core = $2;
	  for (split (/\s*\+\s*/, $_)) {
	    $tmp = "$_-$local_core|".$tmp;
	  }
	}
	$constructions[$cid]{$posid}{core} = $tmp.$constructions[$cid]{$posid}{core};
	$constructions[$cid]{$posid}{core} =~ s/(^|\|)MaybeP/\1DELETE_Prép/g;
      }
    }
  }
  %temp_hash = ();
  %temp_hash2 = ();
  for (split /\|/, $constructions[$cid]{$posid}{core}) {
    next if /^Maybe/;
    if (/^(.*)-/) {
      $temp_hash{$1} = 1;
    }
  }
  for (split /\|/, $constructions[$cid]{$posid}{core}) {
    if (/^MaybeL/) {
      $temp_hash2{$_} = 1;
    } elsif (/^MaybeP/) {
      s/^MaybeP-//;
      for $p (keys %temp_hash) {
	$temp_hash2{"$p-$_"} = 1;
      }
    } else {
      $temp_hash2{$_} = 1;
    }
  }
  $constructions[$cid]{$posid}{core} = join ("|", sort keys %temp_hash2);
  $locs = "";
  $lexinfo =~ /locatifs=\(([^()]*(?:\([^()]*\)[^()]*)*)\)/;
  $locs = $1;
  $locs =~ s/locatif=//;
  for $prep (split (/,locatif=/, $locs)) {
    $prep =~ /id="(\d)"/;
    $prepid = $1;
    $prep =~ /list=\((.*?)\)/;
    $preplist = $1;
    $preplist =~  s/^prep=//;
    for (split (/,prep=/, $preplist)) {
      s/^.*?"//;
      s/".*//;
      next if ($_ eq "~");
      for $cid (0..$#constructions) {
	$posid = $constructions[$cid]{argid2posid}{$prepid};
	next unless $constructions[$cid]{$posid}{core} =~ /((?:MaybeL|Loc)-)(.*?)(\||$)/; # à faire: le cas où on a plusieurs Loc-xxx en alternative (ex Loc:Loc-sn|Loc-sinf)
	$tmp = "";
	while ($constructions[$cid]{$posid}{core} =~ /((?:MaybeL|Loc)-)(.*?)(\||$)/g) {
	  $tmp = "$_-$2|".$tmp;
	}
	$constructions[$cid]{$posid}{core} = $tmp.$constructions[$cid]{$posid}{core};
	$constructions[$cid]{$posid}{core} =~ s/(^|\|)Loc/\1DELETE_Loc/g;
	$constructions[$cid]{$posid}{core} =~ s/(^|\|)MaybeL/\1DELETE_Loc/g;
      }
    }
  }
  # nettoyage des prépositions
  for $cid (0..$#constructions) {
    $posid = -1;
    while (defined($constructions[$cid]{++$posid}{core})) {
      while ($constructions[$cid]{$posid}{core} =~ s/(^|\|)MaybeP-([^\|]+)/\1\2/g) {
      }
      while ($constructions[$cid]{$posid}{core} =~ s/(^|\|)MaybeL-[^\|]+/\1/g) {
      }
      while ($constructions[$cid]{$posid}{core} =~ s/\|Prép.*?(\||$)/\1/g) {
      }
      while ($constructions[$cid]{$posid}{core} =~ s/\|DELETE_(?:Loc|Prép).*?(\||$)/\1/g) {
      }
      $constructions[$cid]{$posid}{core} =~ s/\|DELETE_Loc.*?(\||$)/\1/g;
      $constructions[$cid]{$posid}{core} =~ s/Loc-/loc-/g;
      $constructions[$cid]{$posid}{core} =~ s/Prép-/prép-/g;
      $constructions[$cid]{$posid}{core} =~ s/ /_/g;
      $constructions[$cid]{$posid}{core} =~ s/<E>-//g;
    }
  }

  # Ppv
  for $cid (0..$#constructions) {
    for $posid (keys %{$constructions[$cid]}) {
      next unless $posid =~ /^\d+$/;
      $argid = $constructions[$cid]{posid2argid}{$posid};
      $fs = $constructions[$cid]{$posid}{fs};
      next if $fs eq "0";
      $didsomething = 0;
      if ($fs eq "Obj" && defined($ppv{cla})) {
	$constructions[$cid]{$posid}{core} .= "|cla";
      } elsif ($fs eq "Objà" && defined($ppv{cld})) {
	$constructions[$cid]{$posid}{core} .= "|cld";
      } elsif ($fs eq "Objà" && defined($ppv{y})) {
	$constructions[$cid]{$posid}{core} .= "|y";
      } elsif ($fs eq "Objde" && defined($ppv{en})) {
	$constructions[$cid]{$posid}{core} .= "|en";
      } elsif ($fs eq "Loc" && defined($ppv{y})) {
	$constructions[$cid]{$posid}{core} .= "|y";
      } elsif ($fs eq "Dloc" && defined($ppv{en})) {
	$constructions[$cid]{$posid}{core} .= "|en";
      } elsif (defined($ppv{$argid})) {
	if ($fs eq "Obj") {
	  $constructions[$cid]{$posid}{core} .= "|cla";
	} elsif ($fs eq "Objà") {
	  $constructions[$cid]{$posid}{core} .= "|cld|y";
	} elsif ($fs eq "Objde") {
	  $constructions[$cid]{$posid}{core} .= "|en";
	} elsif ($fs eq "Loc") {
	  $constructions[$cid]{$posid}{core} .= "|y";
	} elsif ($fs eq "Dloc") {
	  $constructions[$cid]{$posid}{core} .= "|en";
	} elsif ($fs =~ /^Obl/) { # aïe, on a une Ppv (inconnue) pour un Obl!!! C'est donc en fait pas un Obl, on va re-deviner ce que c'est
	  if ($constructions[$cid]{$posid}{core} =~ /(^|\||:)\(?sn/) { #pas de prep dans les réals
	    $constructions[$cid]{$posid}{core} .= "|cla";
	    $constructions[$cid]{$posid}{fs} = "Obj";
	  } elsif ($constructions[$cid]{$posid}{core} =~ /(^|\||:)\(?(dans|vers)-sn/) { # un truc en dans-... ou similaires -> Loc (à faire avant le à-...)
	    $constructions[$cid]{$posid}{core} .= "|y";
	    $constructions[$cid]{$posid}{fs} = "Loc";
	  } elsif ($constructions[$cid]{$posid}{core} =~ /(^|\||:)\(?à-(sn|scompl)/) { # un truc en à-... -> Objà
	    $constructions[$cid]{$posid}{core} .= "|cld|y";
	    $constructions[$cid]{$posid}{fs} = "Objà";
	  } elsif ($constructions[$cid]{$posid}{core} =~ /(^|\||:)\(?de-(sn|scompl)/) { # un truc en de-... -> Objde
	    $constructions[$cid]{$posid}{core} .= "|en";
	    $constructions[$cid]{$posid}{fs} = "Objde";
	  } else {
	    $constructions[$cid]{$posid}{core} .= "|y";
	    $constructions[$cid]{$posid}{fs} = "Loc"; # bien risqué...
	  }
	}
      }
    }
  }    

  # on a pu toucher aux fs, et en particulier élminier des Obl. Donc on les renumérote
  for $cid (0..$#constructions) {
    $has_already_an_Obl = 0;
    for $posid (keys %{$constructions[$cid]}) {
      next if ($posid !~ /^\d+$/);
      next unless ($constructions[$cid]{$posid}{fs} =~ /^Obl/);
      if ($has_already_an_Obl == 0) {
	$constructions[$cid]{$posid}{fs} = "Obl";
	$has_already_an_Obl = 1;
      } else {
	$constructions[$cid]{$posid}{fs} = "Obl".++$has_already_an_Obl;
      }
    }
  }


  for $cid (0..$#constructions) {
    for $argid (keys %{$constructions[$cid]}) {
      next unless $argid =~ /^\d+$/;
      while ($constructions[$cid]{$argid}{core} =~ s/(^|\|)([^\|]+?)(\|.*?)?\|\2/\1\2\3/) {
      }				# élimine les réalisations doublons
      $constructions[$cid]{$argid}{core} =~ s/\|\|+/\|/g;
      $constructions[$cid]{$argid}{core} =~ s/^\|//g;
      $constructions[$cid]{$argid}{core} =~ s/\|$//g;
    }
  }


  $id =~ /^._(.*)_\d+$/ || die "Strange id: $id";
  $table = $1;
  #  print STDERR "$reallemma / $table / ex-index: $example{$reallemma}{$table} / ex-table: $example\n";
  if (defined($lemma2infltable{$reallemma})) {
    $infltable = $lemma2infltable{$reallemma};
  } elsif ($cat_tot eq "v" && $suj_vide == 0) {	#($id =~ /^V/) { #entrée verbale et avec sujet (car peut être figée avec sujet figé)
    if ($reallemma =~ /er$/) {
      $infltable = "v-er:std";
    } elsif ($reallemma =~ /ir$/) {
      $infltable = "v-ir2";
    } elsif ($reallemma =~ /vendre$/) {
      $infltable = "v-re3";
      #  } elsif ($reallemma =~ /reclure$/) {
      #    $infltable = "v80";
    } elsif ($reallemma =~ /paraître$/) {
      $infltable = "v67";
    } else {
      print STDERR "WARNING: $id: strange verb $lemma abandoned (no known or guessable inflection table found)\n";
      return;
    }
  } elsif ($id =~ /^[N]/) {	#entrée nominale
    $infltable = "inv";		#entrée sans le verbe, mais comment nc-2f??
  } elsif ($cat_tot eq "cf" || $cat_tot eq "cfi") { #($id =~ /^[C]/) { #entrée figée
    $infltable = "0+";	     #le verbe est présent + les ppv -> on supprime tout
    for ($i=0;$i<$cpt_ppv;$i++) {
      $infltable .= "0+";
    }
  }
  for $cid (0..$#constructions) {
    $constr_str = constr2str($cid);
    while ($constr_str =~ /prép-/) { # sécurité pour 'prép' non codées
      $constr_str =~ s/(Obj(à|de)?|Obl2?|Loc|Dloc|Att):\(?[^>,\)]*prép-[^>,\)]*\)?//; # le sujet doit qd même rester...
      $prep_to_be_encoded{$table}{$reallemma}++;
      $constr_str =~ s/,+/,/g;
      $constr_str =~ s/,$//g;
    }
    next if ($constr_str =~ /prép-/); # si jamais le 'prép' était dans le sujet, ce qui paraît improbable mais bon, sécurité oblige
    ##### HACK: on force tous les Obj à avoir %passif et %ppp_employé_comme_adj (pour les entrées verbales seulement) ######
    if ($id =~ /^V/ && $constr_str =~ /Obj:/ && $base_redistr !~ /\%passif(,|$)/) {
      $base_redistr .= ",\%passif";
    }
    if ($cat eq "V") {
      $base_redistr =~ s/\%passif(,|$)/\%passif,\%ppp_employé_comme_adj\1/;
    }
    ##### FIN HACK ######
    if ($obj_vide == 0 ) {
      $constr_str = "<".$constr_str.">"; #met les <>
    } else {
      $constr_str = "";		#ne les met pas car consruction vide
    }

    #ajout d'un _ après l'identifiant pour garder une séparation avec le numéroPourDésambiguiser dans le résultat de l'analyseur
    print $reallemma."___$id\_\t$infltable\t100;$constructions[$cid]{ppv}$semid;$cat_tot;$constr_str;$macros";
    if (defined($constructions[$cid]{macros})) {
      if ($macros eq "") {	#cat= vide, pas de virgule
        print join(",",@{$constructions[$cid]{macros}});
      } else {
        print ",".join(",",@{$constructions[$cid]{macros}});
      }
    }
    if ($constructions[$cid]{origconstr} eq $base_constr) {
      if ($constr{lightverb} ne "") {
        print ",lightverb=".join("|",sort keys %lightverbs) unless %lightverbs eq ();
      }
      print ";$base_redistr";
      if ($example{$reallemma}{$table} ne "") {
	print "\tEx.: $example{$reallemma}{$table}";
      } elsif ($example ne "") {
 	print "\tEx.: $example";
      }
      print "\t# BASE CONSTR = $base_constr ($base_var_constrs) [$redistr_properties] <".join(" ; ",sort keys %orig_columns)."> ; orig base constr = $orig_base_constr";
    } else {
      if ($constructions[$cid]{lightverb} ne "") {
	if (defined($lightverbs{$constructions[$cid]{lightverb}}) && %lightverbs2 eq ()) {
	  print ",lightverb=".join("|",sort keys %lightverbs);
	  #print STDERR "lv2=$lightverbs\n";
	} elsif (scalar keys %lightverbs2 > 0 && $constructions[$cid]{origconstr} =~ /^N1/) {
	  print ",lightverb=".join("|",sort keys %lightverbs2);
	  #print STDERR "lv3=$lightverbs\n";
	} else {
	  print ",lightverb=$constructions[$cid]{lightverb}";
	  #print STDERR "lv4=$lightverbs\n";
	}
      }
      if ($cat_tot eq "v") {	#entrée verbale
        print ";\%actif";
      } elsif ($cat_tot eq "cf") { #entrée nominale ou figée
        print ";\%default";	   #ou $base_redistr??
      }
      ##### HACK: on force tous les Obj à avoir %passif et %ppp_employé_comme_adj ######
      if ($constr_str =~ /Obj:/) {
	print ",\%passif,\%ppp_employé_comme_adj";
      }
      ##### FIN HACK ######
      if ($example{$reallemma}{$table} ne "") {
	print "\tEx.: $example{$reallemma}{$table}";
      } elsif ($example ne "") {
 	print "\tEx.: $example";
      }
      print "\t# NON-BASE CONSTR = $constructions[$cid]{origconstr} ; orig base constr = $orig_base_constr";
    }
    if ($reallemma ne $lemma) {
      print " ; original lemma = $lemma";
    }
    print "\n";
  }
}
if (scalar keys %prep_to_be_encoded > 0) {print STDERR "WARNING: $id: some entries had non-coded 'prép'. Corresponding syntactic functions have been removed\n";}
for (sort keys %prep_to_be_encoded) {
  print STDERR "\tTable $_\t".join(",",keys %{$prep_to_be_encoded{$_}})."\n";
}

sub constr2str {
  my $i = shift;
  my %constr = %{$constructions[$i]};
  my $result = "";
  my $posid = -1;
  my $already_output_something = 0;
  while (defined($constr{++$posid}{core})) {
    next if $constr{$posid}{fs} eq "0" || $constr{$posid}{fs} eq ""; #l'argument peut avoir été supprimé car faisant partie de l'entrée figée (on saute)
    if ($already_output_something == 1) {#si le premier argument est un argument supprimé
      $result .= ",";
    }
    $result .= $constr{$posid}{fs}.":";
    if ($constr{$posid}{opt} == 1) {
      $result .= "(";
    }
    $result .= $constr{$posid}{core};
    if ($constr{$posid}{opt} == 1) {
      $result .= ")";
    }
    $already_output_something = 1;
  }
  return $result;
}

sub sort_reals {
  my $s = shift;
  my %r = ();
  $s =~ s/^\|//;
  $s =~ s/\|\|+/\|/;
  $s =~ s/\|$//;
  while ($s=~s/^(.+?)(\||$)//) {
    $r{$1} ++;
  }
  return join ("|", sort keys %r);
}

