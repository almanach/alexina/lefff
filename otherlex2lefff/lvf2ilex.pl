#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;
use strict;

my $lefff_ilex = shift;
my %lefff;
my %lefff_suff;

if ($lefff_ilex ne "") {
  open LEFFF, "<$lefff_ilex" || die "Could not open $lefff_ilex: $!";
  binmode LEFFF, ":utf8";
  while (<LEFFF>) {
    chomp;
    next if /^#/;
    s/^(.*?[^\\])\#.*/\1/;
    /^([^\t]+)\t([^\t]+)\t.*/ || next;
    my $citform = $1;
    my $inflclass = $2;
    $citform =~ s/___.*//;
    if (defined($lefff{$citform}) && $lefff{$citform} ne $inflclass) {
      $lefff{$citform} = "v-?";
    } else {
      $lefff{$citform} = $inflclass;
    }
    $citform = "#$citform";
    while ($citform =~ s/^.(.{2,})$/\1/) {
      if (defined($lefff_suff{$citform}) && $lefff_suff{$citform} ne $inflclass) {
	$lefff_suff{$citform} = "v-?";
      } else {
	$lefff_suff{$citform} = $inflclass;
      }
    }
  }
}

my ($lemma, $domain, $subclass, $semdef, $syns, $examples, $syntax, $pron, $pppa_only, $id, $pred, $neg, $pseudo_en, $pseudo_y);

while (<>) {
  chomp;
  ($lemma, $domain, $subclass, $semdef, $syns, $examples, $syntax) = split (/\t/,$_);
  next if $lemma =~ /\(f\.\)$/;
  next if $lemma =~ / (ça|son)$/;
  if ($lemma =~ s/ *\(s\)$//) {$pron = 1} else {$pron = 0}
  if ($lemma =~ s/ *\(ê\)$//) {$pppa_only = 1} else {$pppa_only = 0}
  if ($lemma =~ s/ *\(ne\)$//) {$neg = 1} else {$neg = 0}
  if ($lemma =~ s/ *\(en\)$//) {$pseudo_en = 1} else {$pseudo_en = 0}
  if ($lemma =~ s/ *\(ne en\)$//) {$neg = 1; $pseudo_en}
  if ($lemma =~ s/ *\(y\)$//) {$pseudo_y = 1} else {$pseudo_y = 0}
  if ($lemma =~ s/ 0*([1-9][0-9]*)$//) {$id = $1} else {$id = 1}
  $lemma =~ s/^\s+//; $lemma =~ s/\s+$//;
  $pred = $lemma;
  if ($pron) {
    $pred = "se $pred";
    $pred =~ s/se ([aeiouyé])/s'\1/;
    $pred =~ s/se (h[y])/s'\1/;
  }
  $pred =~ s/^(se |s\')?.*$/\1Lemma/;
  $pred = "en $pred" if $pseudo_en;
  $pred = "y $pred" if $pseudo_y;
  $pred = "ne pas $pred" if $neg;
  my $frame = "";
  my %frames = ();
  my %redistr = ();
  my $frames = "";
  for (split (/\s+/, $syntax)) {
    next if /^\s*$/;
    next if /^P/;
    $frame = "<";
    if (/^[AN]([1-9])([a-q0-9])$/) {
      $frame .= "Suj:".sujcode2sujreal($1);
      if ($2 ne "0" && $2 ne "6") {
	$frame .= ",".circcode2circ($2);
      }
      $frame .= ">";
      $frame .= "en" if $pseudo_en;
      $frame .= "en" if $pseudo_y;
      $frame .= ";cat=v,".sujcode2sujmacros($1);
    } elsif (/^T([1-9])([1-9])([a-q0-9])([a-q0-9])$/) {
      $frame .= "Suj:".sujcode2sujreal($1);
      if ($2 ne "0") {
	$frame .= ",Obj:".objcode2objreal($2);
      }
      if ($3 ne "0" && $3 ne "6") {
	$frame .= ",".circcode2circ($3);
      }
      if ($4 ne "0" && $4 ne "6") {
	$frame .= ",".circcode2circ($4);
      }
      $frame .= ">";
      $frame .= "en" if $pseudo_en;
      $frame .= ";cat=v,".sujcode2sujmacros($1).",".objcode2objmacros($2);
      $frame =~ s/;,/;/; $frame =~ s/,$//;
      $frame =~ s/(Obl.*Obl)/${1}2/;
      $frame =~ s/(Obl2.*Obl)/${1}3/;
      $frame =~ s/(Obl3.*Obl)/${1}4/;
    } else {
      $frame = "";
    }
    $frame .= ",\@neg" if $neg;
    $frame .= ",\@pseudo-en" if $pseudo_en;
    $frame .= ",\@pseudo-y" if $pseudo_y;
    if ($frame ne "") {
      $frames{$_} = $frame;
    }
  }
  for (split (/\s+/, $syntax)) {
    next if /^\s*$/;
    next unless /^P/;
    /^P(.)(.)(..)$/ || die "Bizarre: $_\n";
    my $suj = $1;
    my $obj = $2;
    my $rest = $3;
    my $ok = 0;
    for my $code (keys %frames) {
      if ($suj =~ /^[127]$/ && $obj eq "0" && $code =~ /^T.$suj..$/) {
	  $frames{$code} =~ s/(Obj:\(?)/\1serefl|seréc|/;
	  $ok = 1;
	  last;
      } elsif ($suj =~ /^7$/ && $obj eq "0" && $code =~ /^T.[12]..$/) {
	  $frames{$code} =~ s/(Obj:\(?)/\1seréc|/;
	  $ok = 1;
	  last;
      } elsif ($suj =~ /^8$/ && $obj eq "0" && $code =~ /^T.[23]..$/) {
	  $frames{$code} =~ s/(Obj:\(?)/\1seréc|/;
	  $ok = 1;
	  last;
      } elsif ($code =~ /^T$suj${obj}a.$/ && $rest eq "00") {
	  $frames{$code} =~ s/(Objà:\(?)/\1serefl|/ || die $frames{$code};
	  $ok = 1;
	  last;
      } elsif ($obj eq "0" && $code =~ /^T.$suj..$/) {
	  $redistr{$frames{$code}} .= ",\%se_moyen_ou_neutre";
	  $ok = 1;
	  last;
      } elsif ($obj eq "0" && $suj eq "3" && $code =~ /^T.[49]..$/) {
	  $redistr{$frames{$code}} .= ",\%se_moyen_ou_neutre";
	  $ok = 1;
	  last;
      } elsif ($obj eq "0" && $suj eq "1" && $code =~ /^T.[49]..$/) {
	  $redistr{$frames{$code}} .= ",\%se_moyen_ou_neutre";
	  $ok = 1;
	  last;
      } elsif ($obj eq "0") { # attrape-presque-tout à raffiner
	  $redistr{$frames{$code}} .= ",\%se_moyen_ou_neutre";
	  $ok = 1;
	  last;
      }
    }
    if ($ok == 0) {$syntax =~ s/$_/*$_/; $syntax =~ s/\*\*/*/}
  }
  for (values %frames) {
    print $lemma."___LVF__$id\t";
    if (defined($lefff{$lemma})) {
      print $lefff{$lemma};
    } elsif ($lemma =~ /^(re|sur)(.+)$/ && defined($lefff{$2})) { 
      print $lefff{$2};
    } else {
      if ($lefff_ilex ne "") {
	# pas couvert par le lefff = rare = régulier, au moins dans certains cas
	my $tmp = $lemma;
	my $done = 0;
	while ($tmp =~ s/^.(.{2,})/\1/) {
	  if (defined($lefff_suff{$tmp}) && $lefff_suff{$tmp} ne "v-?") {
	    print $lefff_suff{$tmp};
	    $done = 1;
	    last;
	  }
	}
	if ($done == 0) {
	  print "v-?";
	}
      } else {
	print "v-?";
      }
    }
    print "\t100;";
    print "$pred;v;$_;";
    if ($pppa_only) {
      print "\%ppp_employé_comme_adj";
    } else {
      print "\%actif";
      if (/Obj:/) {print ",\%passif,\%ppp_employé_comme_adj"}
      print $redistr{$_}
    }
    print "\t# $domain\t$subclass\t$semdef\t$syns\t$examples\t$syntax\n";
  }
}


sub sujcode2sujreal {
  my $c = shift;
  if ($c =~ /^[123789]$/) {return "cln|sn"}
  elsif ($c eq "4") {return "cln|scompl|sn"}
  elsif ($c eq "5") {return "cln|scompl|sinf|sn"}
}
sub objcode2objreal {
  my $c = shift;
  if ($c =~ /^[123789]$/) {return "cla|sn"}
  elsif ($c eq "4") {return "cla|scompl|sn"}
  elsif ($c eq "5") {return "cla|scompl|sinf|sn"}
}
sub sujcode2sujmacros {
  my $c = shift;
  my $r;
  if ($c =~ /^[127]$/) {$r = "\@Sujhum"}
  elsif ($c =~ /^[3458]$/) {$r = "\@Sujnonhum"}
  if ($c =~ /^[78]$/) {$r .= ",\@Sujplur"}
  return $r;
}
sub objcode2objmacros {
  my $c = shift;
  my $r;
  if ($c =~ /^[127]$/) {$r = "\@Objhum"}
  elsif ($c =~ /^[3458]$/) {$r = "\@Objnonhum"}
  if ($c =~ /^[78]$/) {$r .= ",\@Objplur"}
  return $r;
}
sub circcode2circ {
  my $c = shift;
  if ($c eq "a") {return "Objà:(à-sn)"}
  elsif ($c eq "b") {return "Obl:(de-sn)"}
  elsif ($c eq "c") {return "Obl:(avec-sn)"}
  elsif ($c eq "d") {return "Obl:(contre-sn)"}
  elsif ($c eq "e") {return "Obl:(par-sn)"}
  elsif ($c eq "g") {return "Loc:(sur-sn|vers-sn)"}
  elsif ($c eq "i") {return "Obl:(de-sn)"}
  elsif ($c eq "j") {return "Loc:(en-sn|dans-sn)"}
  elsif ($c eq "k") {return "Obl:(pour-sn)"}
  elsif ($c eq "l") {return "Obl:(auprès_de-sn)"}
  elsif ($c eq "m") {return "Obl:(devant-sn)"}
  elsif ($c eq "n") {return "Loc:(loc-sn)"}
  elsif ($c eq "q") {return "Obl:(pour-sn)"}
  elsif ($c eq "1") {return "Loc:(loc-sn)"} # où l'on est
  elsif ($c eq "2") {return "Loc:(loc-sn)"} # où l'on va
  elsif ($c eq "3") {return "Dloc:(de-sn)"}
  elsif ($c eq "4") {return "Dloc:(de-sn),Loc:(loc-sn)"} # de ici à là
  elsif ($c eq "5") {return "Obl:(sn)"} # temps (durer deux heures)
  elsif ($c eq "6") {return ""} # modalité
  elsif ($c eq "7") {return "Objde:(de-sn)"} # cause (mourir de)
  elsif ($c eq "8") {return "Obl:(avec-sn|par-sn)"} # instrumental
}
