Lexique syntaxique LGLex-Lefff version 3.4 - 2011/10/05
http://infolingu.univ-mlv.fr/
Licence: LGPL-LR

Le lexique LGLex-Lefff est un lexique syntaxique des verbes et des noms 
prédicatifs du français converti au format Lefff (Tolone & Sagot, 2011) à 
partir du lexique LGLex.
Pour plus de détails sur le lexique Lefff et le formalisme Alexina sur 
lequel il repose, voir (Sagot, 2010).

_____________________________________________
Description du format intensionnel du Lefff :

Chaque entrée du lexique intensionnel correspond à un sens unique du lemme 
correspondant et contient les informations suivantes :
- un identifiant de l'entrée qui indique sa catégorie, la classe (ou table) 
dont elle provient et le numéro de l'entrée dans cette table.
- une classe morphologique, qui définit le modèle qui construit les formes 
fléchies en reposant sur les classes flexionnelles du Lefff ;
- une catégorie ;
- le cadre de sous-catégorisation initial ;
- des informations syntaxiques supplémentaires représentées par des macros ;
- la liste des redistributions possibles ;
- un exemple de phrase contenant l'entrée ;
- un commentaire après le symbole "#".

Par exemple, l'entrée intensionnelle du lexique LGLex-Lefff pour le lemme 
"clouer___V_36SL_28" est comme suit :

clouer___V_36SL_28	v-er:std	100;Lemma;v;<Suj:cln|sn,Obj:sn,Loc:(avec-sn|et-sn|à-sn|sur-sn)>;cat=v;%actif,%passif,%ppp_employé_comme_adj	Ex.: Max a cloué cette planche(avec+contre+sur)celle-là # BASE CONSTR = N0 V N1 Loc N2 (N0 V N1 et N2 ; N0 V N1 Prép N2 ; N0 V N1hum Loc N2abs ; N0 V N1) [[passif par]] <> ; orig base constr = N0 V N1 Loc N2

Cela décrit une entrée transitive avec les informations suivantes :
- son identifiant est V_36SL_28, ce qui correspond à la 28ème entrée de la 
table des verbes de la classe 36SL ;
- sa classe morphologique est "v-er:std", la classe de la conjugaison standard 
des verbes du 1er groupe (se terminant par "-er") ;
- son prédicat sémantique peut être représentée par "Lemma" comme c'est le cas 
ici, c'est-à-dire "clouer" ;
- sa catégorie est "verbe" (v) ;
- elle a trois arguments réalisés canoniquement par la fonction syntaxique Suj 
(sujet), Obj (objet direct) et Loc (argument locatif) ; chaque fonction 
syntaxique est associée à une liste de réalisations possibles, mais le Loc 
est optionnel comme le montre les parenthèses ;
- elle est employée dans trois redistributions différentes : %active, %passive, 
et %ppp_employé_comme_adj ;
- son exemple est Max a cloué cette planche(avec+contre+sur)celle-là.

_____________________________________________
Description du format extensionnel du Lefff :

Le processus de compilation construit une entrée extensionnelle pour chaque 
forme fléchie et chaque redistribution compatible, en flechissant le lemme 
d'après la définition de sa classe morphologique et en appliquant les 
définitions formalisées de ses redistributions.
Par exemple, les seules formes fléchies de "clouer" qui sont compatibles avec 
la redistribution passive sont les formes au participe passé. L'entrée 
extentionnelle du passif pour "cloués" est la suivante (Kmp est l'étiquette 
morphologique pour les formes du participe passé au masculin pluriel) :

cloués	100	v	[pred="clouer___V_36SL_28__1<Suj:sn,Loc:(avec-sn|et-sn|sur-sn|à-sn),Obl2:(par-sn)>",@passive,@pers,cat=v,@Kmp]	clouer___V_36SL_28__1	PastParticiple	Kmp	%passif

L'objet direct original (Obj) a été transformé par un Sujet passif et un Agent 
optionnel (Obl2) réalisé par un syntagme nominal précédé par une préposition 
("par-sn") a été ajouté.

_______________________________________________________________________________
Fonctions syntaxiques, réalisations, redistributions et macros du lexique LGLex-Lefff :

Pour les verbes, le format Lefff format comprend les fonctions syntaxiques 
suivantes :
- Suj pour sujet : la forme clitique est celle d'un clitique nominatif ;
- Obj pour objet direct : la forme clitique est celle d’un clitique accusatif, 
substituable par "ceci/cela", translaté par passivation lorsque c'est possible ;
- Objà pour objet indirect canoniquement introduit par la préposition "à" : 
substituable par "à+pronom non-clitique" mais pas par "ici" ou "là(-bas)", 
cliticisation possible à l’aide du clitique datif ou du clitique locatif "y" ;
- Objde pour objet indirect introduit par la préposition "de" : cliticisation
à l'aide du clitique génitif "en", non substituable par "d'ici" ou "de là" ;
- Loc pour argument locatif : substituable par "ici" ou "là(-bas)", 
cliticisation à l'aide du clitique locatif "y" (ex: "à Paris" dans Pierre 
va à Paris);
- Dloc pour argument délocatif : substituable par "d'ici" ou "de là", 
cliticisation à l'aide du clitique génitif "en" (ex: "de Paris" dans Pierre 
vient de Paris);
- Att pour attribut (du sujet, de l'objet ou de l'"à"-object) et pseudo-objet 
(ex: "3 euros" dans J'ai acheté ceci 3 euros) ;
- Obl et Obl2 pour les autres arguments (non-cliticisables) ; Obl2 est utilisé 
pour les verbes ayant deux arguments obliques, tel que "plaider auprès de 
quelqu'un en faveur de quelqu'un d'autre".

Pour les noms prédicatifs, ils peuvent être portés par un verbe support, le 
même ensemble de fonctions est utilisé.

Pour les verbes et les noms, les réalisations possibles sont de trois sortes :
- pronom clitique : cln pour clitique nominatif (ex: "il" dans Il donne 
ce livre à Marie), cla pour clitique accusatif (ex: "le" dans Il le donne 
à Marie), cld pour clitique datif (ex: "lui" dans Il lui donne ce livre), 
y pour clitique locatif (ex: Max y va), en pour clitique génitif 
(ex: Max en mange);
- syntagme direct : sn pour syntagme nominal (ex: "La belle dame" dans 
La belle dame arrive), sa pour syntagme adjectival (ex: "verte" dans 
La robe est verte), sinf pour syntagme infinitif (ex: "dire aurevoir" 
dans Pierre est parti dire aurevoir), scompl pour syntagme phrastique 
fini (ex: "que Marie est belle" dans Pierre dit que Marie est belle), 
qcompl pour interrogative indirecte (ex: "combien il gagne" in Pierre 
dit combien il gagne) ;
- syntagme prépositionnel : un syntagme direct introduit par une 
préposition (ex: de-sn, de-scompl, pour-sinf).

Pour les verbes, l'inventaire des redistributions possibles est le suivant :
- %actif, une redistribution factice qui n'a presque aucun effet sur les 
informations du cadre de sous-catégorisation initial ;
- %passif pour le passif standard en "par" (ex: Jean assistait Max depuis 
des années-> Max était assisté par Jean depuis des années);
- %passif_de pour le passif en "de" (ex: Marie aime Pierre -> Pierre est 
aimé de Marie) ;
- %actif_impersonnel pour les constructions impersonnelles à l'actif avec 
le sujet inversé, le cas échéant (ex: Un accident est arrivé à Jean -> Il 
est arrivé un accident à Jean);
- %passif_impersonnel pour les constructions impersonnelles au passif avec 
le sujet inversé, le cas échéant (ex: Cette nouvelle information clarifie 
pourquoi Max est faché -> Il est clarifié par cette nouvelle information 
pourquoi Max est faché);
- %ppp_employé_comme_adj pour les participes passés employés comme adjectifs 
(ex: Marie s'assoie -> Marie est assise).

Les noms prédicatifs ont uniquement la construction %default qui construit 
un cadre de sous-catégoriation final identique à l'initial.

Pour les verbes et les noms, les macros representent des informations 
syntaxiques supplémentaires telles que :
- contrôle : par exemple, @CtrlSujObj indique que s'il est réalisé par 
un syntagme infinitif, l'objet est contrôlé par le sujet (ex: "chercher 
du pain" est contrôlé par "Pierre" dans Pierre va chercher du pain);
- mode de la complétive : @SCompInd indique que si le sujet est réalisé 
par un syntagme phrastique fini, son mode est l'indicatif (ex: "qu'il fait 
beau" dans Pierre dit qu'il fait beau) ; au contraire, @CompSubj indique 
que si l'objet direct est réalisé par un syntagme phrastique fini, son mode 
est le subjonctif (ex: "qu'il fasse beau" dans Pierre veut qu'il fasse beau); 
les abréviations sont S pour sujet, rien pour objet direct, A pour objet 
indirect introduit par la préposition "à", De pour objet indirect introduit 
par la préposition "de" ; par défaut, les deux modes sont possibles ;
- syntagme nominal humain ou non humain : @ObjàNhum indique que l'objet 
indirect introduit par la préposition "à" peut dénoter une personne ou un 
animal linguistiquement assimilé à une personne lorsqu'il est réalisé par un 
syntagme nominal (ex: Vercingetorix s’est rendu à l'ennemi) ; au contraire, 
@ObjàN-hum indique que l'objet indirect introduit par la préposition "à" peut 
dénoter un non humain (Jean s’est rendu à mon opinion) ; dans ce cas, @ObjàNhum 
et @ObjàN-hum ne font pas partie de la même entrée car le sens de "se rendre" 
est différent ; dans d'autres cas, par exemple, Max va tomber et Le verre va 
tomber, @SujNhum et @SujN-hum indiquent que le sujet peut dénoté un humain et
un non humain pour la même entrée ; si aucune indication n'est donné pour un 
argument, on peut considérer que les deux sont toujours possibles.

De plus, pour les verbes, les autres macros sont :
Les informations syntaxiques complémentaires les plus intéressantes sont 
représentées sous forme de
- auxiliaire de conjugaison du verbe : @avoir(ex: "achever" dans "Max a achevé 
de peindre le mur") ou @être (ex: "s'arrêter" "Max s'est arrêté de boire") ;
- caractère (essentiellement) pronominal du verbe : @pron (ex:arrêter dans 
l'exemple précédent) ;
- caractère obligatoirement négatif du verbe : @neg (ex: "rajeunir" dans "Que sa 
fille ait 20 ans ne rajeunit pas Max");
- autres clitiques obligatoirement figés au verbe : @pseudo-en (ex: "baver" dans 
"Max en bave avec Luc"), @pseudo-y (ex: le verbe "aller" dans "Luc y va"), 
@pseudo-le (ex: le verbe "disputer" dans "La haine le dispute à la colère"), 
@pseudo-la (ex: "fermer" dans "Fermez-la"), @pseudo-les ("aligner" dans "Max les 
aligne à Luc")~;

____________
Références :

Tolone, Elsa & Sagot, Benoît (2011). Using Lexicon-Grammar tables for French 
verbs in a large-coverage parser. Edité par Z. Vetulani dans Human Language 
Technology, Forth Language and Technology Conference, LTC 2009, Poznán, 
Poland, November 2009, Revised Selected Papers, Lecture Notes in Artificial 
Intelligence (LNAI). Springer Verlag.

Tolone, Elsa (2011). Analyse syntaxique à l'aide des tables du Lexique-Grammaire 
du français. Thèse de doctorat, LIGM, Université Paris-Est. 340 pp.

Sagot, Benoît (2010). The Lefff, a freely available and large-coverage 
morphological and syntactic lexicon for French. Actes du 7ème Language 
Resource and Evaluation Conference (LREC'10), 8 pp. La Valette, Malte.
