#!/usr/bin/env perl

use strict;

my %hash = ();
my %ambiguous_ic = ();

my $ref = shift || die;
open REF, "<$ref" || die;
binmode REF, ":utf8";
while (<REF>) {
  chomp;
  /^(.*?)\t(.*?)\t/;
  my $lemma = $1;
  my $ic = $2;
  my $lemma =~ s/__.*//;
  if (defined($hash{$lemma}) && $hash{$lemma} ne $ic) {
    delete($hash{$lemma});
    $ambiguous_ic{$lemma} = 1;
  } elsif (!defined($ambiguous_ic{$lemma})) {
    $hash{$lemma} = $ic;
  }
}
close REF;

while (<>) {
  chomp;
  /^(.*?)\t(.*?)\t(.*)$/;
  my $orig_lemma = $1;
  my $lemma = $orig_lemma;
  my $ic = $2;
  my $remainder = $3;
  $lemma =~ s/__.*//;
  print "$orig_lemma\t";
  if (defined($hash{$lemma})) {
    print $hash{$lemma};
    if ($ic ne $hash{$lemma}) {
      print STDERR "### changed inflection class for lemma '$lemma': was '$ic' whereas the reference contains '$hash{$lemma}'. Restored '$hash{$lemma}'\n";
    }
  } else {
    print $ic;
  }
  print "\t$remainder\n";
}
