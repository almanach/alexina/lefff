#!/usr/bin/env perl

while (<>) {
    chomp;
    next unless /<orthography>(.+)<\/orthography>/;
    $lemma = $1;
    $_ = <>;
    next unless /<grammaticalCategory>verb<\/grammaticalCategory>/;
    $lemmapref = "";
    if ($lemma =~ s/(se )//) {
	$lemmapref = $1;
    }
    if ($lemma =~ /^[aeiuoé]/) {
      $lemmapref =~ s/^se /s'/;
    }
    print "${lemma}___M__1\tv\t100;${lemmapref}Lemma;v;*;*;*\n";
}
