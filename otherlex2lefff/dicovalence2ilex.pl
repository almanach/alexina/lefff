#!/usr/bin/env perl

# Remarques sur Dicovalence
# 1) PMi (vs. PM): PMi est un attributif, PM ne l'est pas. Test non-pronominal!
#    Mais PMi n'indique pas la/les prépositions utilisées pour la lexicalisation
#    Il est fait mention de ce que "comme" ou "0" sont possible. Mais on trouve aussi "pour", etc...
#    Selon le manuel, la prep devrait être indiquée (dans PQ_PR, ce qui est erronné, mais aucun PMi?_PR
#      n'existe de toute façon)
#    Dans ces cas, ce sont des obliques (en vertu des propriétés de pronominalisation), et
#      l'info d'attribution est indiquée (passer pour: PIVOT P0/P0 [below inf in PP(pour)]
#    Pourquoi l'info d'attribution, dans ce cas, n'est-elle pas indiquée dans les PMi ("nécessairement
#      en rapport avec l'un ou l'autre terme de la construction" : mais lequel???)
# 2) Le "il" des sujets extraposés est-il un pseudo-il? Pourquoi la construction il__sujextrap n'est-elle
#    pas une reformulation au même titre que les reformulations passives (passif être, se passif, se faire passif)?
# 3) Pour le moment, le Lefff ignore totalement les PM (pas le PMi)
# 4) Pour le moment, le Lefff ignore les Ae et les PQ parallèles à 'combien' et 'autant' (ceux qui sont parallèles
#      à 'que' sont des Att)

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my (@funcs,@cfuncs,$pers,$impers,$lemme,%hn,$macros,$moyen,$aux);
my $skip=0;

$usage = <<USAGE;
Usage: dicovalence2ilex.pl [options] {lefff_files} < [dicovalence_file] > [ilex_file]

Input [dicovalence_file] is read on STDIN
Output [ilex_file] is printed on STDOUT
{lefff_files} is a list of .ilex files from the Lefff that are used for knowing the morphological class
  of each entry (no ambiguity resolution yet, hence a problem for the verb 'ressortir').

Options:
(none yet)

USAGE

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^--?h(?:elp)?$/) {
      die $usage;
    }
    else {$lefff_files .= " ".$_}
}
$lefff_files =~ s/^ //;

for $lefff_file (split(/ /,$lefff_files)) {
  open (LEFFF_FILE, "<$lefff_file") || die "Could not open $lefff_file";
  binmode LEFFF_FILE, ":utf8";
  while (<LEFFF_FILE>) {
    /^(.*?)(?:__\S*)?\t(.*?)\t/;
    $lemma2infltable{$1} = $2;
  }
}

while (<>) {
  chomp;
  if (s/^VAL\$\t([^:]+)//) {
    if (!$skip) {
      &output_last_lemma();
    }
    $example = "";
    $lemme=$1;
    $obl2=0;
    $macros="cat=v,";
    $constr="";
    %trad = ();
    @funcs=();
    @cfuncs=();
    @funcs_i=();
    $pers=0;
    $impers=0;
    $impersca=0;
    $pron=0;
    $moyen=0;
    $passivable = 0;
    $pronpref = "";
    if ($lemme=~s/^ne (plus|pas) //) {
      $macros.="\@négatif,";
    }
    if ($lemme=~s/^(s(e |\')) ?//) {
	$pron = 2;
	$pronpref .= $1;
	$macros.="\@pron,";
    }
    if ($lemme=~s/^(\(s[e']\)) ?//) {
	$pron = 1;
	$pronpref .= $1;
	$macros.="\@pron_possible,";
    }
    if ($lemme=~s/^(l(e |\')) ?//) {
      $pronpref .= $1;
      $macros.="\@pseudo-le,";
    }
    if ($lemme=~/^la /) {
      $pronpref .= "la ";
      $macros.="\@pseudo-la,";
    }
    if ($lemme=~/^les /) {
      $pronpref .= "les ";
      $macros.="\@pseudo-les,";
    }
    if ($lemme=~/^en /) {
      $pronpref .= "en ";
      $macros.="\@pseudo-en,";
    }
    if ($lemme=~/^y /) {
      $pronpref .= "y ";
      $macros.="\@pseudo-y,";
    }
    $hn{$lemme}++;
  } elsif (/^NUM\$\t(.*)$/) {
    $num = $1;
  } elsif (/^AUX\$\t(.*)$/) {
    $macros .= "\@$1," unless $1 eq "avoir";
  } elsif (/^VTYPE\$\t(.*)$/) {
    $vtype = $1;
    if ($vtype =~/adjunct_verb/) {
      $skip=1;next;
    }
    if ($vtype =~/copula augmenting/) {
      push(@cfuncs,"Obj:sn|cla");
      push(@cfuncs,"Att:sn|sa");
      $macros.="\@AttObj,";
    } elsif ($vtype =~/copula/) {
      push(@cfuncs,"Suj:sn|cln");
      push(@cfuncs,"Att:sn|sa");
      $macros.="\@AttSuj,";
    }
    $skip=0;
    $vtype =~ s/pseudu/pseudo/; # erreurs dans dicovalence
    if ($vtype=~/pseudo_il/) {
      $impers=1;
    }
    if ($vtype=~/pseudo_ça/) {
      $impersca=1;
    }
    if ($vtype=~/pseudo_se/ && $pron == 0) {
      print STDERR "Doute (1a) pour '$lemme\_$hn{$lemme}' avec pseudo_se : vtype=$vtype  /  pron = 0 turned into 1\n";
      $pron = 1;
    } elsif ($pron > 0 && $vtype!~/pseudo_se/) {
      print STDERR "Doute (1b) pour '$lemme\_$hn{$lemme}' avec pseudo_se : vtype=$vtype  /  pron = 1 (kept)\n";
    }
    for $pseudopro ("en","y","le","la","les","là") {
      if ($vtype=~/pseudo_$pseudopro/ && $macros!~/\@pseudo-$pseudopro/) {
	print STDERR "Doute (2a) pour '$lemme\_$hn{$lemme}' avec pseudo_$pseudopro : vtype=$vtype  /  macros=$macros, adding \@pseudo-$pseudopro\n";
	$macros.="\@pseudo-$pseudopro,";
      } elsif ($macros=~/\@pseudo-$pseudopro/ && $vtype!~/pseudo_$pseudopro/) {
	print STDERR "Doute (2b) pour '$lemme\_$hn{$lemme}' avec pseudo_$pseudopro : vtype=$vtype  /  macros=$macros (keeping \@pseudo-$pseudopro)\n";
      }
    }
  } elsif (/^(P[^\$_]+_PR)\$\t(.*)$/) {
    $preps=$2;
  } elsif (/^EG\$\t(.*)$/) {
    $example = $1;
  }
  if (/^RP\$\t(.*se passif.*)$/) {
    $moyen=1;
  }
  if (/^RP\$\t(.*passif être.*)$/) {
    $passivable=1;
  }
  if (/^TR_(..)\$\t(.*)$/) {
    $trad{$1} = $2;
  }
  if (/^(P[^\$_]+)\$\t(.*)$/) {
    $pros=$2;
    $name=&proton2syntfunc($1);
    next if ($name eq "NULL");
    %reals=();
    %reals_i=();
    if ($name eq "Suj") {
      if ($pros=~/(^| )(je|nous|on|elle|ils?)(,|$)/) {
	$reals{"cln"}++; $pers=1;
      }
      if ($pros=~/(^| )(qu[ei]|ce|ça|ceci|celui-ci|ceux-ci)(,|$)/) {
	$reals{"sn"}++; $pers=1;
      }
      if ($pros=~/(^| )(ça\(inf\))(,|$)/) {
	$reals{"sinf"}++; $pers=1;
      }
      if ($pros=~/(^| )(ça\(de_inf\))(,|$)/) {
	$reals{"de-sinf"}++; $pers=1;
      }
      if ($pros=~/(^| )(ça\(qpsubj\))(,|$)/) {
	$reals{"scompl"}++; $pers=1;
      }
      if ($pros=~/(^| )(ça\(sipind\))(,|$)/) {
	$reals{"qcompl"}++; $pers=1;
      }
      if ($pros=~/(^| )(il_que)(,|$)/) {
	$reals{"sn"}++; $impers=1;
      }
      if ($pros=~/(^| )(il_inf)(,|$)/) {
	$reals{"sinf"}++; $impers=1;
      }
      if ($pros=~/(^| )(il_de_inf)(,|$)/) {
	$reals{"de-sinf"}++; $impers=1;
      }
      if ($pros=~/(^| )(il_à_inf)(,|$)/) {
	$reals{"à-sinf"}++; $impers=1;
      }
      if ($pros=~/(^| )(il_qpind|il_qpsubj)(,|$)/) {
	$reals{"scompl"}++; $impers=1;
      }
    } elsif ($name eq "Obj") {
      if ($pros=~/(^| )(te|vous|le|la|les)(,|$)/) {
	$reals{"cla"}++;
      }
      if ($pros=~/(^| )(qu[ei]|ça|ceci|celui-ci|ceux-ci)(,|$)/) {
	$reals{"sn"}++;
      }
      if ($pros=~/(^| )((?:le|ça)\(inf\))(,|$)/) {
	$reals{"sinf"}++;
      }
      if ($pros=~/(^| )(se réfl\.)(,|$)/) {
	$reals{"seréfl"}++;
      }
      if ($pros=~/(^| )(se réc\.)(,|$)/) {
	$reals{"seréc"}++;
      }
      if ($pros=~/(^| )((?:le|ça)\(de_inf\))(,|$)/) {
	$reals{"de-sinf"}++;
      }
      if ($pros=~/(^| )((?:le|ça)\(à_inf\))(,|$)/) {
	$reals{"à-sinf"}++;
      }
      if ($pros=~/(^| )(?:le|ça)\((qpind|qpsubj)\)(,|$)/) {
	$reals{"scompl"}++;
      }
      if ($pros=~/(^| )(?:le|ça)\((sipind|indirq|indq)\)(,|$)/) { ## A FAIRE: sicompl à côté de scompl et qcompl
	$reals{"qcompl"}++;
      }
      if (defined($reals{cla}) && !defined($reals{sn})) {
	print STDERR "Doute (3) pour '$lemme\_$hn{$lemme}' avec Obj : (adding real 'sn' to existing real 'cla')\n";
	$reals{sn}++;
      }
    } elsif ($name eq "Objà") {
      if ($pros=~/(^| )(me|lui|leur)(,|$)/) {
	$reals{"cld"}++;
      }
      if ($pros=~/(^| )(y)(,|$)/) {
	$reals{"y"}++;
      }
      if ($pros=~/(^| )(quoi|qui|ceci|eux|ça|celui-ci|ceux-ci)(,|$)/) {
	$reals{"à-sn"}++;
      }
      if ($pros=~/(^| )[^ ]+\(à_inf\)(,|$)/) {
	$reals{"à-sinf"}++;
      }
      if ($pros=~/(^| )[^ ]+\((qpind|qpsubj)\)(,|$)/) {
	$reals{"scompl"}++;
      }
      if ($pros=~/(^| )[^ ]+\(à ce (qps|qpi)\)(,|$)/) {
	$reals{"à-scompl"}++;
      }
      if ($pros=~/(^| )(se réfl\.)(,|$)/) {
	$reals{"seréfl"}++;
      }
      if ($pros=~/(^| )(se réc\.)(,|$)/) {
	$reals{"seréc"}++;
      }
    } elsif ($name eq "Objde") {
      if ($pros=~/(^| )(en)(,|$)/) {
	$reals{"en"}++;
      }
      if ($pros=~/(^| )(quoi|qui|ceci|eux|ça|celui-ci|ceux-ci)(,|$)/) { # lui|leur ???
	$reals{"de-sn"}++;
      }
      if ($pros=~/(^| )[^ ]+\(de_inf\)(,|$)/) {
	$reals{"de-sinf"}++;
      }
      if ($pros=~/(^| )[^ ]+\((qpind|qpsubj)\)(,|$)/) {
	$reals{"scompl"}++;
      }
      if ($pros=~/(^| )[^ ]+\(de ce (qps|qpi)\)(,|$)/) {
	$reals{"de-scompl"}++;
      }
      if ($pros=~/(^| )[^ ]+\(sipind\)(,|$)/) {
	$reals{"de-qcompl"}++;
      }
    } elsif ($name eq "Loc") {
      if ($pros=~/(^| )(y)(,|$)/) {
	#		$reals{"dans-sn|en-sn|vers-sn|à-sn|sur-sn|sous-sn|contre-sn"}++;
	$reals{"y"}++;
      }
      #	    if ($pros=~/(^| )([^y].*)(,|$)/) {
      ##		$reals{"dans-sn|en-sn|vers-sn|à-sn|sur-sn|sous-sn|contre-sn"}++;
      #		$reals{"loc-sn"}++;
      #	    }
      if ($pros=~/(^| )([^y].*)(,|$)/) {
# 	if ($preps eq "") {
# 	  $reals{"sn"}++;
# 	} elsif ($preps =~ /\.\.\./) {
# 	  $reals{"loc-sn"}++;
	if ($preps eq "" || $preps =~ /\.\.\./) {
	  $reals{"loc-sn"}++;
	} else {
	  for (split(/, /,$preps)) {
	    if ($_ eq 0) {
	      $reals{"sn"}++;
	    } else {
	      s/ /_/g;
	      if (s/^\((.*)\)(.*)$/$2/) {
		$reals{"$1$2-sn"}++;
	      }
	      $reals{"$_-sn"}++;
	    }
	  }
	}
      }
    } elsif ($name eq "Dloc") {
      if ($pros=~/(^| )(prep .*)(,|$)/) {
	$reals{"de-sn"}++;
      }
      if ($pros=~/(^| )(en)(,|$)/) {
	$reals{"en"}++;
      }
    } elsif ($name eq "Att" || $name eq "Obl") {
      $pros=~s/(^| )(combien|autant)(,|$)//g;
      if ($pros=~/(^| )([^0, ]+)(,|$)/) {
	if ($preps eq "") {
	  $reals{"sn"}++;
	} elsif ($preps =~ /\.\.\./) {
	  $reals{"tps-sn"}++;
	} else {
	  for (split(/\s*[,\/]\s*/,$preps)) {
	    if ($_ eq 0) {
	      $reals{"sn"}++;
	    } else {
	      s/ /_/g;
	      if (s/^\((.*)\)(.*)$/$2/) {
		$reals{"$1$2-sn"}++;
	      }
	      $reals{"$_-sn"}++;
	    }
	  }
	}
      }
    }
    next if ((scalar keys %reals) == 0);
    if ($name eq "Obl") {
      if ($obl2) {
	$name.="2";
      } else {
	$obl2=1;
      }
    }
    if ($name ne "") {
      $reals=join("|",sort {&real2rank($a) <=> &real2rank($b)} keys %reals);
      if ($pers) {
	if ($pros=~/0,/) {
	  push(@funcs,$name.":($reals)");
	} else {
	  push(@funcs,$name.":$reals");
	}
      }
    }
    $preps="";
  }
}
&output_last_lemma();

sub output_last_lemma {
  if ($lemme ne "") {
    $macros =~ s/,$//;
    $output="<".join(",",(@funcs,@cfuncs)).">";
    $output=~s/^<>$//;
    if ($pers == 1) {
      $constr .= ",%actif";
      if ($passivable) {
	$constr.=",%passif";
      }
      if ($moyen) {
	$constr .=",%se_moyen";
      }
    }
    if ($impers == 1) {
      $constr .= ",%actif_impersonnel";
      if ($passivable) {
	$constr.=",%passif_impersonnel";
      }
      if ($moyen) {
	$constr .=",%se_moyen_impersonnel";
      }
    }
    if ($impersca == 1) {
      $constr .= ",%actif_impersonnel_ça";
      if ($passivable) {
	$constr.=",%passif_impersonnel_ça";
      }
      if ($moyen) {
	$constr .=",%se_moyen_impersonnel_ça";
      }
    }
    $constr =~ s/^,//;
    if ($constr eq "") {$constr = "\%actif"}
    $lemme_to_entrynb{$lemme}++;
    $entry = "$lemme\___D__$num\t";
    return if ($lemme =~ / /);
    if (defined $lemma2infltable{$lemme}) {
      $entry .= $lemma2infltable{$lemme};
    } elsif ($lemme =~ /er$/) {
      $entry .= "v-er:std";
    } else {
      print STDERR "Unknown inflection table for lemma $lemme - skipping\n";
      return;
    }
    $entry .= "\t100;${pronpref}Lemma;v;$output;$macros;$constr\t# $example";
    for (sort keys %trad) {
      $entry .= " [$_|$trad{$_}]";
    }
    $entry .="\n"; #$lemme_to_entrynb{$lemme}
    $entry =~ s/^([aeioué].*)\(se\)(.*),\@pron_possible(.*)/\1s'\2,\@pron,\@être\3\n\1\2\3/;
    $entry =~ s/^([^aeioué].*)\(se\)(.*),\@pron_possible(.*)/\1se \2,\@pron,\@être\3\n\1\2\3/;
    print $entry unless $output eq "";
  }
}

sub proton2syntfunc {
    $_=shift;
    if ($_ eq "P0") {return "Suj"}
    elsif ($_ eq "P1") {return "Obj"}
    elsif ($_ eq "P2") {return "Objà"}
    elsif ($_ eq "P3") {return "Objde"}
    elsif ($_ eq "PQ") {return "Att"}
    elsif ($_ eq "PM") {return "NULL"}
    elsif ($_ eq "PMi") {return "Att"}
    elsif ($_ eq "PP") {return "Obl"}
    elsif ($_ eq "PL") {return "Loc"}
    elsif ($_ eq "PDL") {return "Dloc"}
    elsif ($_ eq "PT") {return "Obl"}
    else {return ""}
}

sub real2rank {
  my $r = shift;
  if ($r eq "cln" || $r eq "cla" || $r eq "cld") {return 0}
  if ($r eq "en") {return 1}
  if ($r eq "y") {return 2}
  if ($r =~ /sn$/) {return -1}
  if ($r =~ /sa$/) {return 4}
  if ($r =~ /sinf$/) {return 5}
  if ($r =~ /scompl$/) {return 6}
  if ($r =~ /qcompl$/) {return 7}
}
