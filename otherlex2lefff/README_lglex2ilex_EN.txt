GENERAL INFORMATION
Script: LGLex2ilex
Authors: Elsa Tolone and Benoît Sagot
Organization: Université Paris-Est, LIGM / INRIA Paris-Rocquencourt, ALPAGE
Licence: LGPL
Version: 3.4
Date: 2011/10/05
URL: http://infolingu.univ-mlv.fr/

REFERENCES
Tolone, Elsa & Sagot, Benoît (2011). Using Lexicon-Grammar tables for French 
verbs in a large-coverage parser. Edited by Z. Vetulani in Human Language 
Technology, Forth Language and Technology Conference, LTC 2009, Poznán, 
Poland, November 2009, Revised Selected Papers, Lecture Notes in Artificial 
Intelligence (LNAI). Springer Verlag.

Tolone, Elsa (2011). Analyse syntaxique à l'aide des tables du Lexique-Grammaire 
du français. Thèse de doctorat, LIGM, Université Paris-Est. 340 pp.


DESCRIPTION
The LGLex2ilex script converts the text format of the LGLex lexicon to the
Lefff format. It is implemented in Perl.


USAGE
Usage: lglex2ilex.pl [options] {lefff_files} < [lglex_file] > [ilex_file]
  [lglex_file]
        input file
  [ilex_file]}
        output file
  {lefff_files}
        list of Lefff files with .ilex extension used to know the
        morphological class of each entry

[options]
  -nuc or --no_unknown_construction
        consider that unknown constructions imply the creation of distinct
        secundary entries instead of creating  unknown redistributions that
        can not be deduced from the basic construction

  -e [file] or --examples [file]
        with [file] being a file containing examples for each entry of each
        table, whose format is:
        <lemma><TAB><table_id><TAB><first_example>(<TAB><other_examples>)
        which enables to retrieve examples from this file instead of examples
        from the tables

WARNING: the use of these scripts requires to create an environment variable 
TABLESPATH indicating the path of the root directory containing all the data 
(tables) and the software.

Examples:
  perl lglex2ilex.pl -nuc v.ilex v-phd.ilex < $TABLESPATH/lglex/verbes.lglex.txt > v-lglex.ilex
  perl lglex2ilex.pl -nuc nom.ilex < $TABLESPATH/lglex/noms-predicatifs.lglex.txt > npred-lglex.ilex

The files v.ilex, v-phd.ilex and nom.ilex belong to the Lefff but are only
used here to provide a flexion table. If they are not present, the default
rules are applied so that words whose flexion is unknown. For instance,
a word ending by -er is being attributed the category verb of the first
group, etc. and it is an unvariable word by default.
