#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;

$dv2_file = shift || die;
open DV, "<$dv2_file" || die $!;
binmode DV, ":utf8";

while (<DV>) {
  chomp;
  s/\s+$//;
  if (/^NUM\$\s+(\d+)$/) {
    $num = $1;
  } elsif (s/^FRAME\$\s+//) {
    $origline = $_;
    if (/pseudo_il/) {
      $pseudo_il{$num} = 1;
    }
    for (split /, /) {
      next unless /\[/ && /\[[^\]]/;
      /^(\??)(.+?):.+?:\[(.*)\]$/ || die $_;
      $dubious = $1;
      $sfunc = $2;
      $infos = $3;
      $sfunc =~ s/subj/suj/;
      $sfunc =~ s/objp/obl/;
      $sfunc =~s/^(.)/uc($1)/e;
      $short_sfunc = $sfunc;
      $short_sfunc =~ s/<.*//;
      if (defined($seen_sfunc{$num}) && defined($seen_sfunc{$num}{$short_sfunc})) {
	if (defined($seen_sfunc{$num}{$short_sfunc."2"})) {
	  next; # on ignore les compléments obliques à partir du troisième
	} else {
	  $sfunc =~ s/^([^<]+)/${1}2/;
	  $short_sfunc .= "2";
	}
      }
      $seen_sfunc{$num}{$short_sfunc} = 1;
      for (split /,/, $infos) {
	if (s/^mood://) {
	  $_ = "alt" if $_ eq "ind/subj";
	  s/^(.)/uc($1)/e;
	  if ($sfunc eq "Suj") {
	    $hash{$num}{$sfunc}{"\@SComp$_"} = 1;
	  } elsif ($sfunc eq "Obj") {
	    $hash{$num}{$sfunc}{"\@Comp$_"} = 1;
	  } elsif ($sfunc eq "Objà") {
	    $hash{$num}{$sfunc}{"\@AComp$_"} = 1;
	  } elsif ($sfunc eq "Objde") {
	    $hash{$num}{$sfunc}{"\@DeComp$_"} = 1;
	  } elsif ($sfunc =~ /^Obl/) {
	    print STDERR "Dropping mood information for $sfunc (entry $num)\n";
	  } else {
	    die $sfunc." / ".$_." / ".$origline;
	  }
	} elsif (/^(abs|nhum|hum)$/) {
	  s/^nhum$/cnhum/;
	  $hash{$num}{$sfunc}{"\@$sfunc$_"} = 1;
	} elsif (s/^\?(abs|nhum|hum)$/\1/) {
	  s/^nhum$/cnhum/;
	  $hash{$num}{$sfunc}{"\@$sfunc$_?"} = 1;
	} elsif (/^\+complex$/) {
	  s/^nhum$/cnhum/;
	  $hash{$num}{$sfunc}{"\@${sfunc}complex"} = 1;
	} else {
	  die $_." / ".$origline;
	}
      }
    }
  }
}

my $lefffsfunc;
my $qm;
my $ok;
my ($prep,@prep);
while (<>) {
  chomp;
  $newsprops = "";
  s/\@(Suj|Obj)hum/\@\1Hum/g;
  s/\@(Suj|Obj)nonhum/\@\1Nonhum/g;
  s/\@(Suj|Obj)plur/\@\1Complex/g;
  if (/<link\s+resource="dicovalence"/) {
    /^([^\t]+\t[^\t]+\t[^\t;]+;[^\t;]+;[^\t;]+;)([^;]*);([^;]*)(;.*)$/;
    $start = $1;
    $subcat = $2;
    $sprops = $3;
    $end = $4;
    while (/<link\s+resource="dicovalence"\s+entry="(\d+)"/g) {
      $num = $1;
      for $sfunc (keys %{$hash{$num}}) {
	$sfunc =~ s/(?:le long de\/à travers)/loc/g;
	$ok = 0;
	if (($num == 80930 && $sfunc eq "Objà")
	    || ($num == 13450 && $sfunc eq "Objde")
	    || ($num == 60810 && $sfunc eq "Objà")
	    || ($num == 58515 && $sfunc eq "Objde")
	    || ($num == 68330 && $sfunc eq "Objde")
	    || ($num == 59210 && $sfunc eq "Obj")
	    || ($num == 59210 && $sfunc eq "Obj")
	    || ($num == 3760 && $sfunc eq "Obl<dans>")
	    || ($num == 3370 && $sfunc eq "Obl<dans>")
	    || ($num == 27930 && $sfunc eq "Objà")
	    || ($num == 33610 && $sfunc eq "Obl<au dépens de>") # sic!
	   ) {
	  #erreur de dicovalence
	  next;
	} elsif ((($num == 25790 && $sfunc eq "Obl<de>")
		  )
		 && $subcat =~ /(?:[<,])Obj:/
		) {
	  $lefffsfunc = "Obj";
	  $ok = 1;
	} elsif ((($num == 30090 && $sfunc eq "Obl<sur>")
		  )
		 && $subcat =~ /(?:[<,])Objde:/
		) {
	  $lefffsfunc = "Objde";
	  $ok = 1;
	} elsif ($subcat =~ /(?:[<,])(?:Obl2?|Att|Objde):/ && $sfunc =~ /^Obl2?<(.*)>$/) {
	  $prep = $1;
	  $prep =~ s/ /_/g;
	  @prep = split /[,\/]/, $prep;
	  $ok = 0;
	  if ($subcat =~ /([<,])Obl:/) {
	    $ok = 1;
	    for $prep (@prep) {
	      unless ($subcat =~ /([<,])Obl:(\(?$prep-sn|[^,>]+\|$prep-sn)/) {
		$ok = 0;
		last;
	      }
	    }
	    if ($ok == 1) {
	      $lefffsfunc = "Obl";
	    }
	  }
	  if ($ok == 0 && $subcat =~ /([<,])Obl2:/) {
	    $ok = 1;
	    for $prep (@prep) {
	      unless ($subcat =~ /([<,])Obl2:(\(?$prep-sn|[^,>]+\|$prep-sn)/) {
		$ok = 0;
		last;
	      }
	    }
	    if ($ok == 1) {
	      $lefffsfunc = "Obl2";
	    }
	  }
	  if ($ok == 0 && $subcat =~ /([<,])Att:/) {
	    $ok = 1;
	    for $prep (@prep) {
	      unless ($subcat =~ /([<,])Att:(\(?$prep-s[na]|[^,>]+\|$prep-s[na])/) {
		$ok = 0;
		last;
	      }
	    }
	    if ($ok == 1) {
	      $lefffsfunc = "Att";
	    }
	  }
	  if ($ok == 0 && $subcat =~ /([<,])Objde:/ && !defined($hash{$num}{Objde})) {
	    $ok = 1;
	    for $prep (@prep) {
	      unless ($subcat =~ /([<,])Objde:(\(?$prep-s[na]|[^,>]+\|$prep-s[na])/) {
		$ok = 0;
		last;
	      }
	    }
	    if ($ok == 1) {
	      $lefffsfunc = "Objde";
	    }
	  }
	}
	if ($ok == 0) {
	  if ($subcat =~ /([<,])$sfunc:/) {
	    $lefffsfunc = $sfunc;
	  } elsif ($sfunc eq "Obj" && $subcat =~ /([<,])Att:(\(?sn|[^,>]+\|sn)/) {
	    # vérifier qu'il y a bien sn
	    $lefffsfunc = "Att";
	  } elsif ($sfunc eq "Objde" && $subcat =~ /([<,])Dloc:/ && !defined($hash{$num}{Dloc})) {
	    $lefffsfunc = "Dloc";
	  } elsif ($sfunc eq "Objà" && $subcat =~ /([<,])Loc:/ && !defined($hash{$num}{Loc})) {
	    $lefffsfunc = "Loc";
	  } elsif ($sfunc eq "Obl<de>" && $subcat =~ /([<,])Objde:/) {
	    $lefffsfunc = "Objde";
	  } elsif ($sfunc eq "Obj" && defined($pseudo_il{$num}) && $subcat =~ /([<,])Suj:/) {
	    $lefffsfunc = "Suj";
	  } elsif ($sfunc eq "Obl<loc>" && $subcat =~ /(?:[<,])Loc:/ && !defined($hash{$num}{Loc})) {
	    $lefffsfunc = "Loc";
	  } elsif ($sfunc eq "Obl<dans>" && $subcat =~ /(?:[<,])Loc:(\(?dans-sn|[^,>]+\|dans-sn)/ && !defined($hash{$num}{Loc})) {
	    $lefffsfunc = "Loc";
	  } elsif ($sfunc eq "Obl<sur>" && $subcat =~ /(?:[<,])Loc:(\(?(?:sur|loc)-sn|[^,>]+\|(?:sur|loc)-sn)/ && !defined($hash{$num}{Loc})) {
	    $lefffsfunc = "Loc";
	  } elsif ($sfunc eq "Obl<avec>" && $subcat =~ /(?:[<,])Objà:(\(?avec-sn|[^,>]+\|avec-sn)/ && !defined($hash{$num}{"Objà"})) {
	    $lefffsfunc = "Objà";
	  } elsif ($sfunc eq "Obl<contre>" && $subcat =~ /(?:[<,])Objà:(\(?contre-sn|[^,>]+\|contre-sn)/ && !defined($hash{$num}{"Objà"})) {
	    $lefffsfunc = "Objà";
	  } elsif ($sfunc eq "Obl<à>" && $subcat =~ /([<,])Objà:/ && !defined($hash{$num}{"Objà"})) {
	    $lefffsfunc = "Objà";
	  } elsif ($sfunc eq "Obl2<à>" && $subcat =~ /([<,])Objà:/ && !defined($hash{$num}{"Objà"})) {
	    $lefffsfunc = "Objà";
	  } elsif ($sfunc eq "Obl<à,avec>" && $subcat =~ /([<,])Objà:/ && !defined($hash{$num}{"Objà"})) {
	    $lefffsfunc = "Objà";
	  } elsif ($sfunc eq "Obl<de>" && $subcat =~ /(?:[<,])Dloc:(\(?de-sn|[^,>]+\|de-sn)/ && !defined($hash{$num}{Dloc})) {
	    $lefffsfunc = "Dloc";
	  } else {
	    die $num.": ".$sfunc;
	  }
	}
	for (sort {$a cmp $b} keys %{$hash{$num}{$sfunc}}) {
	  next if /\?$/;
	  next if ($lefffsfunc eq "Obl" && /^\@.*?Comp/);
	  if ($sfunc ne $lefffsfunc) {
	    if (s/^\@$sfunc/\@$lefffsfunc/
		|| ($sfunc eq "Obj" && s/^\@Comp/\@${lefffsfunc}Comp/)
	       ) {
	    } else {
	      die "$num / $sfunc / $lefffsfunc / $_";
	    }
	  }
	  s/^\@$lefffsfunc(.)/"\@$lefffsfunc".uc($1)/e;
	  $qm = quotemeta($_);
	  unless ($newsprops =~ /(^|,)$qm(,|$)/ || $sprops =~ /(^|,)$qm(,|$)/) {
	    $newsprops .= "," unless $newsprops eq "" && $sprops eq "";
	    $newsprops .= $_;
	  }
	}
      }
    }
#    $newsprops =~ s/complex/zzzzzz/gi;
    $newsprops = join ",", sort {$a cmp $b} split /,/, $newsprops;
    while ($newsprops =~ s/\@([A-Z][a-zà2]+?)(Hum|Cnhum|Abs|Complex),\@\1(Hum|Cnhum|Abs|Complex)/\@\1\2_OR_\3/g) {}
#    $newsprops =~ s/zzzzzz/Complex/g;
    $newsprops =~ s/(\@[^,;]+)/\1__DV/g;
    print $start.$subcat.";".$sprops.$newsprops.$end."\n";
  } else {
    print $_."\n";
  }
}
