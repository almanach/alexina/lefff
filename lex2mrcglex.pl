#!/usr/bin/env perl
# $Id: lex2mrcglex.pl 2232 2014-11-30 11:05:51Z clerger $

while (<>) {
    next if /lightverb/;
    chomp;
    s/\t +/\t/;
    s/ \[pred=\'/\t/;
    s/ \[.*\@(poss.)/\t\1\'/;
    s/(.)(?:Se)?<[^>]*>[a-z\-]*\'/\1\'/;
    s/\'.*\@(K?[mf]?[sp]?|[123][smpf]*|W|G|[A-Z]+[0-9]+\w*)[ ,\]].*$/\t\@\1/;
    s/(\@[A-Z]*)([0-9]+)([a-z]+)$/\1\3\2/;
    s/[\'\[].*[,\]].*$//;
#    s/^(.*\@[A-Za-z]+)([123])([123])$/\1\2\n\1\3/;
    s/\@//g;
    s/^([^\t]*\t[^\t]*)$/\1\t/;
    s/^([^\t]*\t[^\t]*\t[^\t]*)$/\1\t/;
    $prevmod=$prev;
    $prevmod=~s/PSs13$/PSs3_/;
    $prev=$_;
    if ($prevmod=~/^(.*PSs)3$/ && /^${1}13$/) {
	s/3$//;
	print "$_\n";	    
    } elsif ($_."_" ne $prevmod) {
	print "$_\n";
    }
}
