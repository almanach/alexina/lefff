;; Mode emacs jouet pour les fichiers ilex - ilex-mode.el
;;
;; Placer ce fichier dans ~/.emacs.d/ et rajouter la ligne
;; (load "~/.emacs.d/ilex-mode.el" t)
;; dans le fichier .emacs (qui est dans ~/)
;;
;; Il peut etre place dans n'importe quel dossier,
;; mais la ligne a rajouter dans le .emacs doit
;; etre modifiee en consequence
;;
;; B. Sagot - (c) 2007-2008


(defvar ilex-mode-hook nil)
(add-to-list 'auto-mode-alist '("\\.ilex\\'" . ilex-mode))
(defconst ilex-font-lock-keywords
  (list
   '("^\\(\\\\#\\|[^\t#]\\)*\t" . font-lock-string-face)
   '("#.*$" . font-lock-comment-face)
   '("%[^,	
]*" . font-lock-constant-face)
   '(";" . font-lock-keyword-face)
   '("\\(Suj\\|Obj�\\|Obja\\|Objde\\|Obj\\|Obl2\\|Obl3\\|Obl\\|Att\\|Loc\\|Dloc\\):" . font-lock-function-name-face)
   '("[<>]" . font-lock-function-name-face)
   )
  "Minimal highlighting expressions for ILEX major mode"
)
(defvar ilex-mode-map
  (let ((ilex-mode-map (make-keymap)))
    ilex-mode-map)
  "Keymap for ILEX major mode")
(defvar ilex-mode-syntax-table
  (let ((ilex-mode-syntax-table (make-syntax-table)))
    (modify-syntax-entry ?\" "w" ilex-mode-syntax-table)
    (modify-syntax-entry ?_ "w" ilex-mode-syntax-table)
    ilex-mode-syntax-table)
  "Syntax table for ilex-mode")

(defun ilex-mode ()
  "Major mode for editing ilex files."
  (kill-all-local-variables)
  (set-syntax-table ilex-mode-syntax-table)
  (use-local-map ilex-mode-map)
  (set (make-local-variable 'font-lock-defaults) '(ilex-font-lock-keywords))
  (setq major-mode 'ilex-mode)
  (setq mode-name "ILEX")
  (run-hooks 'ilex-mode-hook))

;;(define-derived-mode ilex-mode fundamental-mode "ILEX"
;;  "Major mode for editing ilex files."
;;  (set (make-local-variable 'font-lock-defaults) '(ilex-font-lock-keywords))
;;)
(provide 'ilex-mode)
